// **********************************************************************
// Complex numbers in C++
// R. Hiptmair, September 2024
// **********************************************************************

/* SAM_LISTING_BEGIN_1 */
#include <complex>
#include <iostream>
#include <numbers>
using complex = std::complex<double>;
using namespace std::complex_literals;
int main(int /*argc*/, char** /*argv*/) {
  std::cout << "Demo: Complex numbers in C++" << std::endl;
  // This initialization requires std::complex\_literals
  complex z = 0.5; // Std constructor, real part only 
  z += 0.5 + 1.0i;
  // Various elementary operations, see
  // https://en.cppreference.com/w/cpp/numeric/complex
  std::cout << "z = " << z << ", Re(z) = " << z.real()
            << ", Im(z) = " << z.imag() << "| z | = " << std::abs(z)
            << ", arg(z) = " << std::arg(z) << ", conj(z) = " << std::conj(z)
            << std::endl;
  complex w = std::polar(1.0, std::numbers::pi / 4.0);
  std::cout << "w = " << w << std::endl;
  std::cout << "exp(z) = " << std::exp(z)
            << ", abs(exp(z)) = " << std::abs(std::exp(z)) << " = "
            << std::exp(z.real()) << std::endl;
  std::cout << "sqrt(z) = " << std::sqrt(z)
            << ", arg(sqrt(z)) = " << std::arg(std::sqrt(z)) << std::endl;

  return 0;
}
/* SAM_LISTING_END_1 */

/*
  std::cout << "special test\n";
  // Fails with type unsigned int !
  int k = 2;
  int l = 3;
  std::complex<double> u = std::complex<double>(k, l);
  std::complex<double> v = std::complex<double>(k, -l);
  std::complex<double> q = u / v;
  std::cout << " u = " << u << ", v = " << v << ", q = " << q << " = "
            << (u * u) / std::norm(u) << std::endl;
*/
