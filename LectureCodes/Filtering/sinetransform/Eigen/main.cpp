#include "sinetransform.hpp"
#include <Eigen/Dense>
#include <iostream>

int main()
{
  const Eigen::VectorXd y = Eigen::VectorXd::LinSpaced(9, 0, 1);
  Eigen::VectorXd s;

  sinetransform(y, s);
  std::cout << s << std::endl;
  return 0;
}
