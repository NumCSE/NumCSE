///////////////////////////////////////////////////////////////////////////
/// Demonstration code for lecture "Numerical Methods for CSE" @ ETH Zurich
/// (C) 2016 SAM, D-MATH
/// Author(s): N.N.
/// Repository: https://gitlab.math.ethz.ch/NumCSE/NumCSE/
/// Do not remove this header.
//////////////////////////////////////////////////////////////////////////
# include <Eigen/Dense>
# include <array>
# include <cassert>
# include <cmath>
# include <complex> // needed to use complex std::vectors!
# include <unsupported/Eigen/FFT> // contains Eigen::FFT
# include <vector>

namespace chebexp {


using Eigen::VectorXd;
using Eigen::VectorXcd;
using Eigen::Index;

inline
/* SAM_LISTING_BEGIN_0 */
// efficiently compute coefficients \Blue{$\alpha_j$} in the Chebychev expansion
// \Blue{$p = \sum\limits_{j=0}^{n} \alpha_j T_j$} of \Blue{$p\in\Cp_n$} based on values \Blue{$y_k$},
// \Blue{$k=0,\ldots,n$}, in Chebychev nodes \Blue{$t_k$}, \Blue{$k=0,\ldots,n$}
// IN:  values \Blue{$y_k$} passed in \texttt{y}
// OUT: coefficients \Blue{$\alpha_j$}
VectorXd chebexp(const VectorXd& y) {
  const Index n = y.size() - 1;          // degree of polynomial
  const std::complex<double> M_I(0, 1); // imaginary unit
  // create vector \Blue{$\Vz$}, see \eqref{eq:tpipcext}
  VectorXcd b(2*(n + 1));
  const std::complex<double> om = -M_I*(M_PI*static_cast<double>(n))/static_cast<double>(n+1); 
  for (int j = 0; j <= n; ++j) {
    b(j) = std::exp(om*static_cast<double>(j))*y(j); // this cast to double is necessary!!
    b(2*n+1-j) = std::exp(om*static_cast<double>(2*n+1-j))*y(j);
  }
  
  // Solve linear system \eqref{eq:tpiplse} with effort \com{$O(n\log n)$}
  Eigen::FFT<double> fft;  // \eigen's helper class for DFT
  VectorXcd c = fft.inv(b); // -> c = ifft(z), inverse fourier transform 
  // recover \Blue{$\beta_j$}, see \eqref{eq:tpiplse}
  VectorXd beta(c.size());
  const std::complex<double> sc = M_PI_2/static_cast<double>(n + 1)*M_I;
  for (unsigned j = 0; j < c.size(); ++j) {
    beta(j) = ( std::exp(sc*static_cast<double>(-n+j))*c[j] ).real();
  }
  // recover \Blue{$\alpha_j$}, see \eqref{eq:tpdft}
  VectorXd alpha = 2*beta.segment(n,n); alpha(0) = beta(n);
  return alpha;
}
/* SAM_LISTING_END_0 */

// Conversion from Chebychev basis to monomial basis
// Argument a provides coefficients in Chebychev basis
// returns vector of monomial coefficients 
inline VectorXd chebexpToMonom(const VectorXd &a) {
  const Index n = a.size() -1; // degree of polynomial
  assert(n >= 0); 
  // vector for returning monomial coefficients of polynomials
  VectorXd r{ VectorXd::Zero(n+1) };
  r[0] = a[0];
  if (n > 0) {
    r[1] = a[1];
    if (n > 1) {
      // monomial coefficients of Chebychev polynomials 
      VectorXd c0 = VectorXd::Zero(n+1);  // Lowest-degree Chebychev polynomial is constant = 1
      c0[0] = 1.0;
      VectorXd c1 = VectorXd::Zero(n+1);  // First-degree Chebychev polynomial is t -> t
      c1[1] = 1.0;
      
      for (int j=2; j<=n; ++j) {
        const bool even = j%2 == 0;
        VectorXd &c = even ? c0 : c1;
        VectorXd &otherC = even ? c1 : c0;
        c[0] *= -1.0;
        for (int k=1; k <= j; ++k) {
          c[k] = 2*otherC[k-1] - c[k];
        }
        r += a[j]*c;
      }
    }
  }
  return r;
}


} //namespace chebexp
