///////////////////////////////////////////////////////////////////////////
/// Demonstration code for lecture "Numerical Methods for CSE" @ ETH Zurich
/// (C) 2024 SAM, D-MATH
/// Author(s): Ralf Hiptmair
/// Repository: https://gitlab.math.ethz.ch/NumCSE/NumCSE/
/// Do not remove this header.
//////////////////////////////////////////////////////////////////////////

#include <malloc.h>

#include <Eigen/Core>
#include <Eigen/Sparse>
#include <cstddef>
#include <cstdio>
#include <iostream>

/**
 * Retrieve total size of memory allocated on heap so far
 */
size_t total_allocated_space(void) {
  struct mallinfo2 mi = mallinfo2();
  return mi.uordblks;
}

/**
 * Display statistics on heap usage
 * https://man7.org/linux/man-pages/man3/mallinfo.3.html
 */

void display_mallinfo2(void) {
  struct mallinfo2 mi;

  mi = mallinfo2();

  std::printf("\tTotal non-mmapped bytes (arena):       %zu\n", mi.arena);
  std::printf("\t# of free chunks (ordblks):            %zu\n", mi.ordblks);
  std::printf("\t# of free fastbin blocks (smblks):     %zu\n", mi.smblks);
  std::printf("\t# of mapped regions (hblks):           %zu\n", mi.hblks);
  std::printf("\tBytes in mapped regions (hblkhd):      %zu\n", mi.hblkhd);
  std::printf("\tMax. total allocated space (usmblks):  %zu\n", mi.usmblks);
  std::printf("\tFree bytes held in fastbins (fsmblks): %zu\n", mi.fsmblks);
  std::printf("\tTotal allocated space (uordblks):      %zu\n", mi.uordblks);
  std::printf("\tTotal free space (fordblks):           %zu\n", mi.fordblks);
  std::printf("\tTopmost releasable block (keepcost):   %zu\n", mi.keepcost);
}

/**
 * Initialize some sparse matrix
 */
Eigen::SparseMatrix<double> generateTestMatrix(Eigen::Index N) {
  Eigen::SparseMatrix<double> A(N, N);
  A.reserve(Eigen::RowVectorXi::Constant(N, 3));
  for (Eigen::Index j = 0; j < N; ++j) {
    A.insert(j, j) = 2.0;
    A.insert(j, (j + 1) % N) = -1.0;
    A.insert(j, (j + 2) % N) = 1.0;
  }
  return A;
}

int main(int /*argc*/, char** /*argv*/) {
  std::cout << "At start of program: " << total_allocated_space() << " byte\n";

  for (Eigen::Index N : {10000, 100000, 1000000, 10000000, 100000000}) {
    std::cout << "N = " << N << ": \n";
    const Eigen::SparseMatrix<double> A = generateTestMatrix(N);
    std::cout << "\t after matrix allocation: " << total_allocated_space() << " byte\n";
    {
      std::cout << "I. auto expression templates\n";
      const Eigen::VectorXd y = Eigen::VectorXd::Constant(N,1.0);
      Eigen::VectorXd x(N);
      std::cout << "\t after vector allocation: " << total_allocated_space() << " byte\n";
      malloc_stats();
      auto tmp = A*y;
      std::cout << "\t after auto tmp = A*y: " << total_allocated_space() << " byte\n";
      x += tmp;
      std::cout << "\t after x += tmp: " << total_allocated_space() << " byte\n";
    }
    {
      std::cout << "I. One expression update \n";
      const Eigen::VectorXd y = Eigen::VectorXd::Constant(N,1.0);
      Eigen::VectorXd x(N);
      std::cout << "\t after vector allocation: " << total_allocated_space() << " byte\n";
      Eigen::VectorXd tmp = A*y;
      std::cout << "\t after  Eigen::VectorXd tmp = A*y;: " << total_allocated_space() << " byte\n";
      x += tmp;
      std::cout << "\t after x += tmp: " << total_allocated_space() << " byte\n";
    }
    std::cout << "\t At end of loop body: " << total_allocated_space() << " byte\n";
  }
  std::cout << "At end of program: " << total_allocated_space() << " byte\n";
  display_mallinfo2();
  return 0;
}

/*  Lieber Ralf,

entschuldige die etwas traege Antwort. Spontan wuerde ich es mit valgrind
(heap profiler) versuchen. Im Anhang ein kleines Beispiel inklusive Makefile
(damit es laeuft muss valgrind installiert sein; auf den ada Rechnern geht es).
Valgrind liefert die user / total benutze heap-mem wie im angehaengten massif.txt.

Lass mich wissen ob ich weiter helfen kann. (Leider bin ich bis etwa 13:00
mit dem Auto unterwegs, aber danach wieder voll erreichbar.)

Viele Gruesse & Schoenen Tag

Roger
________________________________________
From: Hiptmair  Ralf <ralfh@ethz.ch>
Sent: Friday, December 20, 2024 7:50 AM
To: Käppeli  Roger
Subject: C++: Laufzeitinformation über Heapallocation ?

Lieber Roger,

wie kann man während des Laufs eines C++-Programms verfolgen, wieviel
dynamischer Speicher auf dem Heap gebraucht wird?

Ich habe es mit mallinfo2() versucht, doch es liefert keine brauchbaren
Ergebnisse, da es auf irgendwelche (mir unklaren) "Arenas" Bezug nimmt.
Was es in 'uordblks' zurückgibt ist auch nicht kompatibel damit, was
malloc_stats() druckt.

Hast Du eine Lösung für dieses Problem?

Danke.

Ralf
*/
