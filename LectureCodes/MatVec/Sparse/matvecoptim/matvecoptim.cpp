///////////////////////////////////////////////////////////////////////////
/// Demonstration code for lecture "Numerical Methods for CSE" @ ETH Zurich
/// (C) 2024 SAM, D-MATH
/// Author(s): Ralf Hiptmair
/// Repository: https://gitlab.math.ethz.ch/NumCSE/NumCSE/
/// Do not remove this header.
//////////////////////////////////////////////////////////////////////////

#include <Eigen/Dense>
#include <Eigen/Sparse>
#include <chrono>
#include <iomanip>
#include <iostream>
#include <limits>

Eigen::SparseMatrix<double> generateTestMatrix(Eigen::Index N) {
  Eigen::SparseMatrix<double> A(N, N);
  A.reserve(Eigen::RowVectorXi::Constant(N, 3));
  for (Eigen::Index j = 0; j < N; ++j) {
    A.insert(j, j) = 2.0;
    A.insert(j, (j + 1) % N) = -1.0;
    A.insert(j, (j + 2) % N) = 1.0;
  }
  return A;
}

/* SAM_LISTING_BEGIN_1 */
enum Timing_Mode { ALLOC, DIRECT, EXPRTEMPL, AUTO };
unsigned int matop_timing(const Eigen::SparseMatrix<double> &A,
                          const Eigen::VectorXd &y, Timing_Mode mode,
                          unsigned int reps = 3) {
  const Eigen::Index N = A.rows();
  assert((N == A.cols()) && ("Matrix must be square"));
  assert((N == y.size()) && ("Wrong vector size"));
  Eigen::VectorXd x = Eigen::VectorXd::Constant(N, 1.0);

  using time_point_t = std::chrono::high_resolution_clock::time_point;
  using duration_t = std::chrono::nanoseconds;
  unsigned int time = std::numeric_limits<unsigned int>::max();
  for (unsigned int k = 0; k < reps; ++k) {
    // Part of the code whose runtime is measured
    time_point_t start = std::chrono::high_resolution_clock::now();
    if (mode == DIRECT) {
      for (Eigen::Index k = 0; k < A.rows(); k++) {
        for (typename Eigen::SparseMatrix<double>::InnerIterator it(A, k); it;
             ++it) {
          x[it.row()] += it.value() * x[it.col()];
        }
      }
    } else if (mode == EXPRTEMPL) {
      x += A * y;
    } else if (mode == ALLOC) {
      const Eigen::VectorXd tmp = (A * y).eval();
      x += tmp;
    } else {
      auto tmp = A * y;
      x += tmp;
    }
    time_point_t end = std::chrono::high_resolution_clock::now();
    // END: runtime measurement
    for (Eigen::Index i = 0; i < N; ++i) {
      x[i] += y[i] * y[i];
    }
    duration_t elapsed = std::chrono::duration_cast<duration_t>(end - start);
    auto elapsed_time = elapsed.count();
    time = (time > elapsed_time) ? elapsed_time : time;
  }
  return time;
}
/* SAM_LISTING_END_1 */

int main(int /*argc*/, char ** /*argv*/) {
  for (Eigen::Index N : {10000, 100000, 1000000, 10000000, 100000000}) {
    const Eigen::SparseMatrix<double> A{generateTestMatrix(N)};
    const Eigen::VectorXd y = Eigen::VectorXd::Random(N);
    double time_direct = matop_timing(A, y, DIRECT) / 1000.0;
    double time_exprtempl = matop_timing(A, y, EXPRTEMPL) / 1000.0;
    double time_alloc = matop_timing(A, y, ALLOC) / 1000.0;
    double time_auto = matop_timing(A, y, AUTO) / 1000.0;
    std::cout << std::setprecision(2) << "N = " << N
              << ": time(exprtempl) = " << time_exprtempl
              << " ms, time(direct) = " << time_direct
              << " ms, time(auto) = " << time_auto
              << " ms, time(alloc) = " << time_alloc << " ms\n";
  }
  /*
build_release $ ./bin/LectureCodes/MatVec/Sparse/matvecoptim/matvecoptim 
N = 10000: time(exprtempl) = 61 ms, time(direct) = 52 ms, time(auto) = 77 ms, time(alloc) = 77 ms
N = 100000: time(exprtempl) = 3.3e+02 ms, time(direct) = 6.4e+02 ms, time(auto) = 4.9e+02 ms, time(alloc) = 3.6e+02 ms
N = 1000000: time(exprtempl) = 3.6e+03 ms, time(direct) = 2.7e+03 ms, time(auto) = 3.9e+03 ms, time(alloc) = 3.6e+03 ms
N = 10000000: time(exprtempl) = 6.6e+04 ms, time(direct) = 2.7e+04 ms, time(auto) = 6.9e+04 ms, time(alloc) = 6.6e+04 ms
N = 100000000: time(exprtempl) = 6.5e+05 ms, time(direct) = 2.6e+05 ms, time(auto) = 6.9e+05 ms, time(alloc) = 6.5e+05 ms

   */
  return 0;
}
