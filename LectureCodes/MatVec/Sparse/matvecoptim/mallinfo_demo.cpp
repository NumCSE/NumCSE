///////////////////////////////////////////////////////////////////////////
/// Demonstration code for lecture "Numerical Methods for CSE" @ ETH Zurich
/// (C) 2024 SAM, D-MATH
/// Author(s): Ralf Hiptmair
/// Repository: https://gitlab.math.ethz.ch/NumCSE/NumCSE/
/// Do not remove this header.
//////////////////////////////////////////////////////////////////////////

#include <malloc.h>

#include <Eigen/Core>
#include <cstdio>
#include <iostream>
#include <vector>

void display_mallinfo2(void) {
  struct mallinfo2 mi;

  mi = mallinfo2();

  std::printf("\tTotal non-mmapped bytes (arena):       %zu\n", mi.arena);
  std::printf("\t# of free chunks (ordblks):            %zu\n", mi.ordblks);
  std::printf("\t# of free fastbin blocks (smblks):     %zu\n", mi.smblks);
  std::printf("\t# of mapped regions (hblks):           %zu\n", mi.hblks);
  std::printf("\tBytes in mapped regions (hblkhd):      %zu\n", mi.hblkhd);
  std::printf("\tMax. total allocated space (usmblks):  %zu\n", mi.usmblks);
  std::printf("\tFree bytes held in fastbins (fsmblks): %zu\n", mi.fsmblks);
  std::printf("\tTotal allocated space (uordblks):      %zu\n", mi.uordblks);
  std::printf("\tTotal free space (fordblks):           %zu\n", mi.fordblks);
  std::printf("\tTopmost releasable block (keepcost):   %zu\n", mi.keepcost);
}

int main(int /*argc*/, char** /*argv*/) {
  std::cout << "At start of program\n";
  display_mallinfo2();
  {
    std::cout << "\n I. Allocation of std vectors\n";
    std::vector<std::vector<double>> vecs;
    for (int i = 0; i < 5; ++i) {
      vecs.emplace_back(1000, 0.5 * i);
      std::cout << "After " << i + 1 << " allocations:\n";
      display_mallinfo2();
    }
  }
  std::cout << "==================================================\n";
  {
    std::cout << "\nII. Allocation of Eigen vectors\n";
    for (int i = 0; i < 5; ++i) {
      std::vector<Eigen::VectorXd> vecs;
      vecs.push_back(Eigen::VectorXd::Constant(1000, 0.5 * i));
      std::cout << "After " << i + 1 << " allocations:\n";
      display_mallinfo2();
    }
  }
  std::cout << "At end of program\n";
  display_mallinfo2();
  return 0;
}
