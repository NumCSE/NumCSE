#include <time.h>  //header for clock()

#include <iostream>

/*
 * simple Timer Class, using clock()-command from the time.h (should work on all
 * platforms) this class will only report the cputime (not walltime)
 */
class simpleTimer {
 public:
  simpleTimer() = default;
  void start() {
    time = clock();
    bStarted = true;
  }
  [[nodiscard]] double getTime() const {
    assert(bStarted);
    return static_cast<double>(clock() - time) / static_cast<double>(CLOCKS_PER_SEC);
    ;
  }
  void reset() {
    time = 0;
    bStarted = false;
  }

 private:
  clock_t time {0};
  bool bStarted {false};
};
