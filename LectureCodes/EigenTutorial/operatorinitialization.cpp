// **********************************************************************
// Eigen tutorial codes: http://eigen.tuxfamily.org/dox/GettingStarted.html
// Demo of use of initialization operator in Eigen
// https://eigen.tuxfamily.org/dox/group__TutorialAdvancedInitialization.html
// **********************************************************************

#include <Eigen/Core>
#include <iostream>

int main(int /*argc*/, char ** /*argv*/) {
  std::cout << "Demo for comma operator initialization in Eigen\n";
  Eigen::VectorXd v(10);
  for (int i = 0; i < 10; ++i) {
    v[i] = static_cast<double>(i);
  }
  std::cout << "I)   v = " << v.transpose() << std::endl;
  v.head(5) << 0.6,1.2,1.8,2.4,3.0;
  std::cout << "II)  v = " << v.transpose() << std::endl;
  Eigen::VectorXd x(4);
  x << 0.3,1.3,2.3,3.3;
  v.tail(4) << (x.reverse() + Eigen::VectorXd::Constant(4,10.0));
  std::cout << "III) v = " << v.transpose() << std::endl;
  // Error: Too few coefficient passed to comma operator
  // v.head(5) << -1.5,-2.5;
  v.head(4) = x;
  std::cout << "IV)  v = " << v.transpose() << std::endl;
  return 0;
}
