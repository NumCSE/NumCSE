#pragma once

#include <Eigen/Dense>
#include <Eigen/Eigenvalues>

/* Golub-Welsch algorithm for computing Gauss points */

/* SAM_LISTING_BEGIN_0 */
struct QuadRule {
  Eigen::VectorXd nodes, weights;
} __attribute__((aligned(32)));

inline QuadRule gaussquad_(const unsigned int n) {
  QuadRule qr;
  Eigen::MatrixXd M = Eigen::MatrixXd::Zero(n, n);
  for (unsigned int i = 1; i < n; ++i) {
    const double b = i / std::sqrt(4. * i * i - 1.);
    M(i, i - 1) = b;
    // M(i - 1, i ) = b is optional as the EV-Solver only references
    // the lower triangular part of M
  }
  // using Eigen's built-in solver for symmetric eigenvalue problems
  const Eigen::SelfAdjointEigenSolver<Eigen::MatrixXd> eig(M);

  qr.nodes = eig.eigenvalues();  //
  qr.weights =
      2 * eig.eigenvectors().topRows<1>().array().pow(2);
  return qr;
}
/* SAM_LISTING_END_0 */
