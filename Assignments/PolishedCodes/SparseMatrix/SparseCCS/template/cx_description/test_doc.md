# Tests of sparseCCS.hpp

> 

***
> `std::tuple<Eigen::VectorXd, Eigen::VectorXi, Eigen::VectorXi> CCS(const MatrixXd & A)`: We test this function on the matrix A defined by

```
4, -1,  0, -1,  0,  0,
-1,  4, -1,  0, -1, 0,
0, -1,  4,  0,  0, -1,
-1,  0,  0,  4, -1, 0,
0, -1,  0, -1,  4, -1,
0,  0, -1,  0, -1,  4;

```
> The tests are passed if the l^2 error of the vectors val, row_ind and col_ptr in the returned tuple are < 10^{-8}. The reference values are computed with the built-in

`Eigen::SparseMatrix<double> As = A.sparseView();
As.makeCompressed();`