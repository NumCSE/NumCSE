/* **********************************************************************
 * Course "Numerical Methods for CSE", R. Hiptmair, SAM, ETH Zurich
 * Author: R. Hiptmair
 * Date: October 2024
 */

#include <Eigen/Dense>
#include <cmath>
#include <iostream>

namespace NumQuadFormulas {
/* SAM_LISTING_BEGIN_2 */
Eigen::VectorXd compWeightsPolyQR_fast(const Eigen::VectorXd& nodes, double a,
                                       double b) {
  // Number of quadrature nodes
  const unsigned int n = nodes.size();
  // Matrix for linear system
  Eigen::MatrixXd M(n, n);
  // First row all ones
  M.row(0) = Eigen::RowVectorXd::Constant(n, 1.0);
  // Fill remaining rows
  for (int j = 1; j < n; ++j) {
    M.row(j) = (M.row(j - 1).array() * nodes.transpose().array()).matrix();
  }
  // Set up right-hand side vector
  Eigen::VectorXd rhs(n);
  double b_pow_j = b;
  double a_pow_j = a;
  for (int j = 1; j <= n; ++j, a_pow_j *= a, b_pow_j *= b) {
    rhs[j - 1] = (b_pow_j - a_pow_j) / j;
  }
  return M.lu().solve(rhs);
}
/* SAM_LISTING_END_2 */

/* SAM_LISTING_BEGIN_1 */
Eigen::VectorXd compWeightsPolyQR(const Eigen::VectorXd& nodes, double a,
                                  double b) {
  // Number of quadrature nodes
  const unsigned int n = nodes.size();
  // Matrix for linear system of equations providing weights
  Eigen::MatrixXd M(n, n);
  for (int i = 0; i < n; ++i) {
    for (int j = 0; j < n; ++j) {
      M(i, j) = std::pow(nodes[j], i);
    }
  }
  // Set up right-hand side vector
  Eigen::VectorXd rhs(n);
  double b_pow_j = b;
  double a_pow_j = a;
  for (int j = 1; j <= n; ++j, a_pow_j *= a, b_pow_j *= b) {
    rhs[j - 1] = (b_pow_j - a_pow_j) / j;
  }
  return M.lu().solve(rhs);
}
/* SAM_LISTING_END_1 */

}  // namespace NumQuadFormulas

int main(int /*argc*/, char** /*argv*/) {
  std::cout << "Demo codes: Numerical Quadrature Formulas" << std::endl;

  std::cout << "Newton Cotes Quadrature formulas\n";
  for (int k = 2; k < 8; ++k) {
    std::cout << k << "-point formula, weights = "
              << NumQuadFormulas::compWeightsPolyQR(
                     Eigen::VectorXd::LinSpaced(k, 0.0, 1.0), 0.0, 1.0)
                     .transpose()
              << std::endl;
  }
  std::cout << "5-point Gauss quadrature rule: ";
  Eigen::VectorXd q =
      (Eigen::VectorXd(5) << 0.0000000000000000, -0.538469310105683,
       0.5384693101056831, -0.906179845938664, 0.9061798459386640)
          .finished();
  std::cout << "weights = " << NumQuadFormulas::compWeightsPolyQR(q, -1.0, 1.0)
            << std::endl;
  return 0;
}
