#ifndef CINFTRF_HPP
#define CINFTRF_HPP

/* **********************************************************************
 * Course "Numerical Methods for CSE", R. Hiptmair, SAM, ETH Zurich
 * Author: R. Hiptmair
 * Date: December 2024
 */

#include <Eigen/Dense>
#include <Eigen/Eigenvalues>
#include <iomanip>
#include <iostream>
#include <ostream>

// See Code 7.4.2.24 {cpp:gaussquad} in lecture document
/* SAM_LISTING_BEGIN_0 */
struct QuadRule {
  QuadRule() = default;
  QuadRule(const QuadRule &) = default;
  QuadRule(QuadRule &&) = default;
  QuadRule& operator = (const QuadRule &) = default;
  QuadRule& operator = (QuadRule &&) = default;
  explicit QuadRule(unsigned int n):nodes_(n), weights_(n) {}
  ~QuadRule() = default;
  Eigen::VectorXd nodes_, weights_;
} __attribute__((aligned(32)));

inline QuadRule gaussquad(const unsigned int n) {
  QuadRule qr;
  // Symmetric matrix whose eigenvalues provide Gauss points
  Eigen::MatrixXd M = Eigen::MatrixXd::Zero(n, n);
  for (unsigned int i = 1; i < n; ++i) {  // \Label[line]{gw:3}
    const double b = i / std::sqrt(4. * i * i - 1.);
    M(i, i - 1) = M(i - 1, i) = b;
  }  // \Label[line]{gw:3x}
  // using Eigen's built-in solver for symmetric eigenvalue problems
  const Eigen::SelfAdjointEigenSolver<Eigen::MatrixXd> eig(M);

  qr.nodes_ = eig.eigenvalues();  // Gauss quadrature nodes as eigenvalues!
  qr.weights_ = 2 * eig.eigenvectors().topRows<1>().array().pow(2);  // \Label[line]{gw:4}
  return qr;
}
/* SAM_LISTING_END_0 */

/**
 * Compute the constant c involved in the transformation (7.18.2.4) by quadrature
 *
 * @param n the number of gauss quadrature nodes used
 *
 */
/* SAM_LISTING_BEGIN_2 */
double findConst(unsigned int n) {
  // Obtain nodes and weights of n-points Gauss Legendre quadrature rule on $[-1,1]$.
  QuadRule qr{gaussquad(n)};
  double c = 0.0;
  // TODO: (7-18.b) compute the constant c by quadrature
  //
  // START
  // Transform to quadrature formula on $[0,1]$. Affinely shift nodes and
  // rescale weights.
  qr.nodes_ = (qr.nodes_ + Eigen::VectorXd::Constant(n, 1.0)) / 2.0;
  qr.weights_ /= 2.0;
  // Integrand: function $\psi$ from \prbeqref{eq:moll}
  auto psi = [](double s) -> double { return std::exp(-1.0 / (s * (1 - s))); };
  // Evaluation of quadrature formula
  for (unsigned int j = 0; j < n; ++j) {
    c += qr.weights_[j] * psi(qr.nodes_[j]);
  }
  c = 1.0 / c;
  // END
  return c;
}
/* SAM_LISTING_END_2 */


/**
 * Compute the quadrature rule stated in (7.18.2.7)
 *
 * @param n the number of quadrature nodes used
 */
/* SAM_LISTING_BEGIN_3 */
QuadRule compLogQuadRule(unsigned int n) {
  // Obtain weights and nodes of Gauss Legendre quadrature rule
  const QuadRule glqr{gaussquad(n)};
  // Nodes and weights to be computed
  QuadRule logqr(n);
  // Scaling constant in \prbeqref{eq:phi}
  const double c = findConst(n);
  // TODO: (7-18.d) Compute weights and nodes as given in \prbeqref{eq:wp1} and
  // \prbeqref{eq:wp2}. Approximate integrals by means of $n$-point
  // Gauss-Legendre quadrature.
  //
  // START
  // Integrand: function $\psi$ from \prbeqref{eq:moll}
  auto psi = [](double s) -> double { return std::exp(-1.0 / (s * (1 - s))); };
  for (unsigned int j = 0; j < n; ++j) {
    // Approximate integral of $\psi$ from $0$ to $\frac12(\wh{c}_j+1)$
    // First transform Gauss-Legendre
    const double a = 0.0;
    const double b = 0.5 * (glqr.nodes_[j] + 1);
    const double w_fac = 0.5 * (b - a);
    // Apply transformed Gauss-Legendre quadrature to compute values of $\Phi$,
    // recall \lref{rem:quadtrf}.
    double phival = 0.0;
    for (unsigned int k = 0; k < n; ++k) {
      phival += w_fac * glqr.weights_[k] *
                psi(a + 0.5 * (b - a) * (glqr.nodes_[k] + 1));
    }
    phival *= c;
    // Formula \prbeqref{eq:wp2}
    logqr.nodes_[j] = phival;
    // Formula \prbeqref{eq:wp1}
    logqr.weights_[j] = 0.5 * c * glqr.weights_[j] * std::log(phival) *
                        psi(0.5 * (glqr.nodes_[j] + 1));
  }
  // END
  return logqr;
}
/* SAM_LISTING_END_3 */
#endif
