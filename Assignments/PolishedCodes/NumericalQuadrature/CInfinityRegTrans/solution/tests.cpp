#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN

#include "doctest.h"
#include "cinfinitytrf.hpp"

TEST_SUITE("CinfinityRegularizingTransform") {
  
  TEST_CASE("double findConst" * doctest::description("compare with reference")) {
    const double ref = 142.2503757340146;
    const double stud = findConst(50);
    CHECK((ref - stud) == doctest::Approx(0.).epsilon(1e-4));
  }
  
  TEST_CASE("QuadRule compLogQuadRule" 
    * doctest::description("compare with true value")) {
    const unsigned int p = 3, n = 20;
    double L_exact = -1.0 / ((p + 1.0) * (p + 1.0));
    const QuadRule logqr{compLogQuadRule(n)};
    double s = 0.0;
    for (unsigned int k = 0; k < n; ++k) {
      s += logqr.weights_[k] * std::pow(logqr.nodes_[k], p);
    }
    CHECK((s - L_exact) == doctest::Approx(0.).epsilon(1e-4));
  }

}