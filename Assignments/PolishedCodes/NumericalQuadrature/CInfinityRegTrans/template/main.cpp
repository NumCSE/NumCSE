#include "cinfinitytrf.hpp"

int main(int /*argc*/, char ** /*argv*/) {
  std::cout << "Numerical Quadrature: Regularization by Transformation\n";
  
  // Check the constant c
  std::cout << "Finding constant c:\n";
  for (unsigned int n : {5, 10, 15, 20, 25, 30}) {
    std::cout << " n = " << n << ": c = " << std::setprecision(16)
              << findConst(n) << std::endl;
  }
  std::cout << std::endl;
  
  // Test the modified quadrature rule for the case f(t) = t
  const QuadRule logqr{compLogQuadRule(20)};
  double s = 0.0;
  for (unsigned int k = 0; k < 20; ++k) {
    s += logqr.weights_[k] * logqr.nodes_[k];
  }
  std::cout << "Compute integral of logt * t over [0,1] with 20-point:\n" 
            << "exact solution = -0.25, " 
            << "numerical solution = " << s << std::endl;  
  return 0;
}