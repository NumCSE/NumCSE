#pragma once

#include <Eigen/Dense>
#include <cassert>
#include <iostream>
#include <utility>

#include "makeA.hpp"


/* SAM_LISTING_BEGIN_1 */
Eigen::VectorXd data_fit_normal(const Eigen::VectorXd &t,
                                const Eigen::VectorXd &f) {
    const Eigen::MatrixXd A = make_A(t);
    const Eigen::VectorXd b = f;
    const int m = A.rows();
    const int n = A.cols();
    Eigen::VectorXd gamma = Eigen::VectorXd::Zero(n);
    // TODO: solve gamma by normal equation method
    // START
    gamma = (A.transpose() * A).lu().solve(A.transpose() * b);
    // END
    return gamma;
}
/* SAM_LISTING_END_1 */

/* SAM_LISTING_BEGIN_2 */
Eigen::VectorXd data_fit_xnormal(const Eigen::VectorXd &t,
                                 const Eigen::VectorXd &f) {
    const Eigen::MatrixXd A = make_A(t);
    const Eigen::VectorXd b = f;
    const int m = A.rows();
    const int n = A.cols();
    Eigen::VectorXd gamma = Eigen::VectorXd::Zero(n);
    // TODO: solve gamma by extended normal equation method
    // START
    Eigen::MatrixXd xmat(m + n, m + n);
    xmat << - Eigen::MatrixXd::Identity(m, m), A, A.transpose(), Eigen::MatrixXd::Zero(n,n);
    Eigen::VectorXd rhs(m + n);
    rhs << b, Eigen::VectorXd::Zero(n);
    gamma = xmat.lu().solve(rhs).tail(n);
    // END
    return gamma;
}
/* SAM_LISTING_END_2 */

/* SAM_LISTING_BEGIN_3 */
Eigen::VectorXd data_fit_qr(const Eigen::VectorXd &t,
                            const Eigen::VectorXd &f) {
    const Eigen::MatrixXd A = make_A(t);
    const Eigen::VectorXd b = f;
    const int m = A.rows();
    const int n = A.cols();
    Eigen::VectorXd gamma = Eigen::VectorXd::Zero(n);
    // TODO: solve gamma by QR-decomposition
    // START
    Eigen::MatrixXd Ab(m, n + 1); Ab << A, b;
    Eigen::MatrixXd R = Ab.householderQr().matrixQR().template triangularView<Eigen::Upper>();
    Eigen::MatrixXd R_nn = R.block(0, 0, n, n);
    gamma = R_nn.lu().solve(R.block(0, n, n, 1));
    // END
    return gamma;
}
/* SAM_LISTING_END_3 */

/* SAM_LISTING_BEGIN_4 */
Eigen::VectorXd data_fit_svd(const Eigen::VectorXd &t,
                            const Eigen::VectorXd &f) {
    const Eigen::MatrixXd A = make_A(t);
    const Eigen::VectorXd b = f;
    const int m = A.rows();
    const int n = A.cols();
    Eigen::VectorXd gamma = Eigen::VectorXd::Zero(n);
    // TODO: solve gamma by SVD
    // START
    Eigen::JacobiSVD<Eigen::MatrixXd> svd(A, Eigen::ComputeThinU | Eigen::ComputeThinV);
    Eigen::VectorXd sv = svd.singularValues();
    Eigen::MatrixXd U = svd.matrixU();
    Eigen::MatrixXd V = svd.matrixV();
    gamma = V * (sv.cwiseInverse().asDiagonal()) * (U.leftCols(n).transpose() * b);
    // END
    return gamma;
}
/* SAM_LISTING_END_4 */
