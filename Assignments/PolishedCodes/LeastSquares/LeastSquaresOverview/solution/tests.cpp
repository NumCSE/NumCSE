#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN
#include <Eigen/Dense>

#include "doctest.h"
#include "datafit.hpp"

TEST_SUITE("") {
  TEST_CASE("normal equation method" * doctest::description("")) {
    Eigen::VectorXd t(4);
    t << -1, 0, 1, 2;
    Eigen::VectorXd f(4);
    f << 1, 1, 3, 3;
    Eigen::VectorXd stud = data_fit_normal(t, f);
    Eigen::VectorXd sol(3); sol << 1.6, 0.8, 0;
    
    CHECK((stud - sol).norm() == doctest::Approx(0.).epsilon(1e-4));
  }
  
  TEST_CASE("extended normal equation method" * doctest::description("")) {
    Eigen::VectorXd t(4);
    t << -1, 0, 1, 2;
    Eigen::VectorXd f(4);
    f << 1, 1, 3, 3;
    Eigen::VectorXd stud = data_fit_xnormal(t, f);
    Eigen::VectorXd sol(3); sol << 1.6, 0.8, 0;
    
    CHECK((stud - sol).norm() == doctest::Approx(0.).epsilon(1e-4));
  }
  
  TEST_CASE("qr" * doctest::description("")) {
    Eigen::VectorXd t(4);
    t << -1, 0, 1, 2;
    Eigen::VectorXd f(4);
    f << 1, 1, 3, 3;
    Eigen::VectorXd stud = data_fit_qr(t, f);
    Eigen::VectorXd sol(3); sol << 1.6, 0.8, 0;
    
    CHECK((stud - sol).norm() == doctest::Approx(0.).epsilon(1e-4));
  }
  
  TEST_CASE("svd" * doctest::description("")) {
    Eigen::VectorXd t(4);
    t << -1, 0, 1, 2;
    Eigen::VectorXd f(4);
    f << 1, 1, 3, 3;
    Eigen::VectorXd stud = data_fit_svd(t, f);
    Eigen::VectorXd sol(3); sol << 1.6, 0.8, 0;
    
    CHECK((stud - sol).norm() == doctest::Approx(0.).epsilon(1e-4));
  }
}
