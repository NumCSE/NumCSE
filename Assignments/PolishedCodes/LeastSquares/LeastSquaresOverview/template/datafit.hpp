#pragma once

#include <Eigen/Dense>
#include <cassert>
#include <iostream>
#include <utility>

#include "makeA.hpp"


/* SAM_LISTING_BEGIN_1 */
Eigen::VectorXd data_fit_normal(const Eigen::VectorXd &t,
                                const Eigen::VectorXd &f) {
    const Eigen::MatrixXd A = make_A(t);
    const Eigen::VectorXd b = f;
    const int m = A.rows();
    const int n = A.cols();
    Eigen::VectorXd gamma = Eigen::VectorXd::Zero(n);
    // TODO: solve gamma by normal equation method
    // START

    // END
    return gamma;
}
/* SAM_LISTING_END_1 */

/* SAM_LISTING_BEGIN_2 */
Eigen::VectorXd data_fit_xnormal(const Eigen::VectorXd &t,
                                 const Eigen::VectorXd &f) {
    const Eigen::MatrixXd A = make_A(t);
    const Eigen::VectorXd b = f;
    const int m = A.rows();
    const int n = A.cols();
    Eigen::VectorXd gamma = Eigen::VectorXd::Zero(n);
    // TODO: solve gamma by extended normal equation method
    // START

    // END
    return gamma;
}
/* SAM_LISTING_END_2 */

/* SAM_LISTING_BEGIN_3 */
Eigen::VectorXd data_fit_qr(const Eigen::VectorXd &t,
                            const Eigen::VectorXd &f) {
    const Eigen::MatrixXd A = make_A(t);
    const Eigen::VectorXd b = f;
    const int m = A.rows();
    const int n = A.cols();
    Eigen::VectorXd gamma = Eigen::VectorXd::Zero(n);
    // TODO: solve gamma by QR-decomposition
    // START

    // END
    return gamma;
}
/* SAM_LISTING_END_3 */

/* SAM_LISTING_BEGIN_4 */
Eigen::VectorXd data_fit_svd(const Eigen::VectorXd &t,
                            const Eigen::VectorXd &f) {
    const Eigen::MatrixXd A = make_A(t);
    const Eigen::VectorXd b = f;
    const int m = A.rows();
    const int n = A.cols();
    Eigen::VectorXd gamma = Eigen::VectorXd::Zero(n);
    // TODO: solve gamma by SVD
    // START

    // END
    return gamma;
}
/* SAM_LISTING_END_4 */
