#include <Eigen/Dense>
#include <iostream>

#include "datafit.hpp"

int main() {
  // f(t) = g1 + g2 * t + g3 * t^2 where g1 = g2 = g3 = 1 
  Eigen::VectorXd t(3);
  t << -1, 0, 1;
  Eigen::VectorXd f(3);
  f << 1, 1, 3;
  
  Eigen::VectorXd gamma_nml  = data_fit_normal(t, f);
  Eigen::VectorXd gamma_xnml = data_fit_xnormal(t, f);
  Eigen::VectorXd gamma_qr   = data_fit_qr(t, f);
  Eigen::VectorXd gamma_svd  = data_fit_svd(t, f);
  std::cout << "solution by normal eq. method:\n" << gamma_nml.transpose() << "\n";
  std::cout << "solution by extended normal eq. method:\n" << gamma_xnml.transpose() << "\n";
  std::cout << "solution by QR:\n" << gamma_qr.transpose() << "\n";
  std::cout << "solution by SVD:\n" << gamma_svd.transpose() << "\n";
  
  return 0;
}
