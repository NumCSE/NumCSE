#include <Eigen/Dense>

Eigen::MatrixXd make_A(const Eigen::VectorXd &t) {
  const int m = t.size();
  const int n = 3;
  Eigen::MatrixXd A(m, n);
  for (int i = 0; i < m; i++) {
    for (int j = 0; j < n; j++) {
      A(i,j) = std::pow(t(i), j);
    }
  }
  return A;
}
