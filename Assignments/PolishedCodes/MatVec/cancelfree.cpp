/* **********************************************************************
 * Course "Numerical Methods for CSE", R. Hiptmair, SAM, ETH Zurich
 * Author: R. Hiptmair
 * Date: October 2024
 */

#include <cmath>
#include <iostream>

namespace CancelFree {

/* SAM_LISTING_BEGIN_1 */
std::pair<double, double> cancelProne(double x) {
  auto f = [](double x) -> double {
    const double t = std::sin(x);
    return (1.0 - std::cos(x)) / (t * t);
  };
  auto g = [](double x) -> double {
    const double w = std::pow(1 + x * x, 1.0 / 3.0);
    return (w - 1.0) / (x * x);
  };
  return {f(x), g(x)};
}
/* SAM_LISTING_END_1 */

/* SAM_LISTING_BEGIN_2 */
std::pair<double, double> cancelFree(double x) {
  // Original: $f(x) = \frac{1-\cos x}{\sin^2x}$
  auto f = [](double x) -> double {
    const double t = std::cos(x);
    return 1.0 / (1.0 + t);
  };
  // Alternative cancellation-free implementation
  auto f_alt = [](double x) -> double {
    double t = std::sin(x / 2.0) / std::sin(x);
    return 2 * t * t;
  };
  // Original $g(x) = \frac{\left(1+x^2\right)^{1/3}-1}{x}$
  auto g = [](double x) -> double {
    const double w = std::pow(1 + x * x, 1.0 / 3.0);
    return 1.0 / (w * (w + 1.0) + 1.0);
  };
  return {f(x), g(x)};
}
/* SAM_LISTING_END_2 */

}  // namespace CancelFree

int main(int /*argc*/, char** /*argv*/) {
  std::cout << "Demo: Avoiding cancellation" << std::endl;

  for (double x : {1.0, 0.1, 0.01, 1E-5, 1E-10}) {
    auto [fc, gc] = CancelFree::cancelProne(x);
    auto [ff, gf] = CancelFree::cancelFree(x);
    std::cout << "x = " << x << ", cancelProne = [" << fc << ", " << gc
              << "] , cancelFree = [ " << ff << ", " << gf << " ]\n ";
  }

  return 0;
}
