# Tests of rkintegrator.hpp

***
> We test `step()` function for $f(x)=x^2$ with forward Euler and Heun's third order method; we test `solve()` function for $f(x) = x^2$ with forward Euler.
***
