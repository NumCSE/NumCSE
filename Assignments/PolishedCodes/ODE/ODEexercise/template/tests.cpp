#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN
#include <Eigen/Dense>

#include "doctest.h"
#include "rkintegrator.hpp"

TEST_SUITE("rkintegrator") {

  TEST_CASE("test step()" * doctest::description("test step() for f(x) = x^2 by forward Euler")) {
    Eigen::MatrixXd A(1,1); A(0,0) = 0;
    Eigen::VectorXd b(1); b(0) = 1;
    auto f = [](Eigen::VectorXd x)->Eigen::VectorXd {return x.array() * x.array();};
    RKIntegrator<Eigen::VectorXd, decltype(f)> forward_euler(f, A, b);
    double stud = (forward_euler.step(0.01, (Eigen::VectorXd(1)<<1.0).finished()))(0);
    double sol  = 1.01;
    CHECK(std::abs(sol - stud) == doctest::Approx(0.).epsilon(1e-4));
  }
  
  TEST_CASE("test step()" * doctest::description("test step() for f(x) = x^2 by Heun's third-order method")) {
    Eigen::MatrixXd A(3,3); A << 0, 0, 0, 1./3., 0, 0, 0, 2./3., 0;
    Eigen::VectorXd b(3); b << 1./4., 0.0, 3./4.;
    auto f = [](Eigen::VectorXd x)->Eigen::VectorXd {return x.array() * x.array();};
    RKIntegrator<Eigen::VectorXd, decltype(f)> heun3(f, A, b);
    double stud = (heun3.step(0.01, (Eigen::VectorXd(1)<<1.0).finished()))(0);
    double k0 = 1.0;
    double k1 = pow(1.0 + 1./3.*0.01*k0, 2);
    double k2 = pow(1.0 + 2./3.*0.01*k1, 2);
    double sol = 1.0 + 1./4. * 0.01 * k0 + 3./4. * 0.01 * k1;
    CHECK(std::abs(sol - stud) == doctest::Approx(0.).epsilon(1e-4));
  }
  
  TEST_CASE("test solve()" * doctest::description("test solve() for f(x) = x^2 by forward Euler")) {
    Eigen::MatrixXd A(1,1); A(0,0) = 0;
    Eigen::VectorXd b(1); b(0) = 1;
    auto f = [](Eigen::VectorXd x)->Eigen::VectorXd {return x.array() * x.array();};
    RKIntegrator<Eigen::VectorXd, decltype(f)> forward_euler(f, A, b);
    double stud = ((forward_euler.solve(0.02, (Eigen::VectorXd(1)<<1.0).finished(), 2)).back())(0);
    double sol  = 1.01 * 1.01 * 0.01 + 1.01;
    CHECK(std::abs(sol - stud) == doctest::Approx(0.).epsilon(1e-4));
  }
}
