#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN
#include <Eigen/Dense>
#include <Eigen/Eigenvalues>
#include <iomanip>

#include "doctest.h"
#include "leapfrog.hpp"

TEST_SUITE("LeapFrogOscillator") {
  TEST_CASE("void PolyEval::addPoint" *
            doctest::description("Testing output")) {
    Eigen::VectorXd v, y;
    Eigen::VectorXd ones = Eigen::VectorXd::Ones(5);
    // Generating Random initial conditions in (0,1)
    Eigen::VectorXd v0 = 0.5 * (Eigen::VectorXd::Random(5) + ones);
    Eigen::VectorXd y0 = 0.5 * (Eigen::VectorXd::Random(5) + ones);
    // Potential energy for simple harmonic oscillator V(y) = 1/2*y.norm()^2
    auto gradV = [](Eigen::VectorXd y) { return y; };
    // Timestepping using the leapFrog method
    std::tie(y, v) = leapFrog(gradV, y0, v0, 1, 100);
    // Checking the output size
    REQUIRE(y.size() == 5);
    REQUIRE(v.size() == 5);
    // Computing the values at final time analytically
    Eigen::ArrayXd tan_theta = y0.array() / v0.array();
    Eigen::ArrayXd theta = tan_theta.atan();
    Eigen::ArrayXd amplitudes = y0.array() / theta.sin();
    // Values at final time
    Eigen::ArrayXd final_y = amplitudes * (theta + 1).sin();
    Eigen::ArrayXd final_v = amplitudes * (theta + 1).cos();
    // Comparing leapfrog output with analytical solution
    CHECK((y - final_y.matrix()).norm() == doctest::Approx(0.).epsilon(1e-3));
    CHECK((v - final_v.matrix()).norm() == doctest::Approx(0.).epsilon(1e-3));
  }

  TEST_CASE("void testCVGLeapfrog" *
            doctest::description(
                "Not tested. Run the main executable to see its output")) {}
}
