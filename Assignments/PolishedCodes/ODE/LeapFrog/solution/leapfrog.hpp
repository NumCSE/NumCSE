/* **********************************************************************
 * Course "Numerical Methods for CSE", R. Hiptmair, SAM, ETH Zurich
 * Author: R. Hiptmair
 * Date: December 2022
 */

#define _USE_MATH_DEFINES

#include <Eigen/Dense>
#include <Eigen/QR>
#include <Eigen/SVD>
#include <cassert>
#include <cmath>
#include <exception>
#include <iomanip>
#include <iostream>
#include <limits>
#include <stdexcept>
#include <string>
#include <tuple>
#include <vector>

#include "Eigen/src/Core/Matrix.h"

/* SAM_LISTING_BEGIN_1 */
template <typename GVFUNCTOR>
std::pair<Eigen::VectorXd, Eigen::VectorXd> leapFrog(GVFUNCTOR &&gradV,
                                                     const Eigen::VectorXd &y0,
                                                     const Eigen::VectorXd &v0,
                                                     double T, unsigned int M) {
  // Timestep size
  const double h = T / M;
  // Vector storing the terms $\cob{\Vy_k}$
  Eigen::VectorXd y{y0};
  // Vector storing the state compoents $\cob{\Vv_{k+\frac12}}$
  // This is the special initial step \prbeqref{eq:sic}
  Eigen::VectorXd v{v0 - 0.5 * h * gradV(y)};
  // Main timestepping loop implementing \prbeqref{eq:lf}
  for (unsigned int k = 0; k < M; ++k) {
    y += h * v;  // $\cob{\Vy_{k+1}=\Vy_{k} + h \Vv_{k+\frac12}}$
    v -= h * gradV(y);  // $\cob{\Vv_{k+\frac32}=\Vv_{k+\frac12} - h\grad V(\Vy_{k})}$
  }
  return {y, v + 0.5 * h * gradV(y)};  // See \prbeqref{eq:VM}
}
/* SAM_LISTING_END_1 */
// clang-format on

/* SAM_LISTING_BEGIN_7 */
template <typename GVFUNCTOR>
void testCVGLeapfrog(GVFUNCTOR &&gradV, const Eigen::VectorXd &y0,
                     const Eigen::VectorXd &v0, double T) {
  const unsigned int no_exp = 8;  // Number of times the stepsize is halved
  unsigned int M = 16;            // Smallest number of timesteps
  // Vectors for storing final approximate states
  std::vector<Eigen::VectorXd> yT;
  std::vector<Eigen::VectorXd> vT;
  // Run leapfrog with various timesteps
  for (unsigned int j = 0; j < no_exp; ++j, M *= 2) {
    auto [y, v] = leapFrog(gradV, y0, v0, T, M);
    yT.push_back(y);
    vT.push_back(v);
  }
  // Finally run leapfrog with 1/10 the timestep to obtain a reference solution
  auto [y_ref, v_ref] = leapFrog(gradV, y0, v0, T, 10 * M);
  // Tabulate the Euclidean vector norms of the final errors
  M = 16;
  double y_errp;
  double v_errp;
  for (unsigned int l = 0; l < yT.size(); ++l, M *= 2) {
    const double y_errn = (y_ref - yT[l]).norm();
    const double v_errn = (v_ref - vT[l]).norm();
    std::cout << "M= " << M << ": |yM-y(T)| = " << y_errn
              << ", |vM-v(T)| = " << v_errn;
    if (l > 0) {
      std::cout << ", ratio y = " << y_errp / y_errn
                << ", ratio v = " << v_errp / v_errn;
    }
    std::cout << std::endl;
    y_errp = y_errn;
    v_errp = v_errn;
  }
}
/* SAM_LISTING_END_7 */

void numExpCvgLeapfrog(void) {
  // Test of convergence for $\cob{N=1}$, $\cob{V(y) = \frac14 y^4}$
  Eigen::VectorXd y0(1);
  y0[0] = 1.0;
  Eigen::VectorXd v0(1);
  v0[0] = 0.0;
  auto gradV = [](const Eigen::VectorXd &y) -> Eigen::VectorXd {
    return (Eigen::VectorXd(y.size()) << y[0] * y[0] * y[0]).finished();
  };
  double T = 10;
  testCVGLeapfrog(gradV, y0, v0, T);
}
