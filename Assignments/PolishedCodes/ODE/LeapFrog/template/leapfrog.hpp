/* **********************************************************************
 * Course "Numerical Methods for CSE", R. Hiptmair, SAM, ETH Zurich
 * Author: R. Hiptmair
 * Date: December 2022
 */

#define _USE_MATH_DEFINES

#include <Eigen/Dense>
#include <Eigen/QR>
#include <Eigen/SVD>
#include <cassert>
#include <cmath>
#include <exception>
#include <iomanip>
#include <iostream>
#include <limits>
#include <stdexcept>
#include <string>
#include <tuple>
#include <vector>

#include "Eigen/src/Core/Matrix.h"

// clang-format off
/* SAM_LISTING_BEGIN_1 */
template <typename GVFUNCTOR>
std::pair<Eigen::VectorXd, Eigen::VectorXd> leapFrog(GVFUNCTOR &&gradV,
                                                     const Eigen::VectorXd &y0,
                                                     const Eigen::VectorXd &v0,
                                                     double T, unsigned int M) {
  Eigen::VectorXd y,v;
   // TODO : Implement the leapfrom scheme and return y_M and v_M
   // START

   return {y,v};
   // END
}
/* SAM_LISTING_END_1 */
// clang-format on

/* SAM_LISTING_BEGIN_7 */
template <typename GVFUNCTOR>
void testCVGLeapfrog(GVFUNCTOR &&gradV, const Eigen::VectorXd &y0,
                     const Eigen::VectorXd &v0, double T) {
   // TODO : Print a table of values which reveals the algebraic rate of
   //        convergence of the error at the final time step
   // START

   // END
}
/* SAM_LISTING_END_7 */

void numExpCvgLeapfrog(void) {
  // Test of convergence for $\cob{N=1}$, $\cob{V(y) = \frac14 y^4}$
  Eigen::VectorXd y0(1);
  y0[0] = 1.0;
  Eigen::VectorXd v0(1);
  v0[0] = 0.0;
  auto gradV = [](const Eigen::VectorXd &y) -> Eigen::VectorXd {
    return (Eigen::VectorXd(y.size()) << y[0] * y[0] * y[0]).finished();
  };
  double T = 10;
  testCVGLeapfrog(gradV, y0, v0, T);
}
