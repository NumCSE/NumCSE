/* **********************************************************************
 * Course "Numerical Methods for CSE", R. Hiptmair, SAM, ETH Zurich
 * Author: R. Hiptmair
 * Date: December 2022
 */

#define _USE_MATH_DEFINES

#include "leapfrog.hpp"
#include <Eigen/Dense>
#include <Eigen/QR>
#include <Eigen/SVD>
#include <cassert>
#include <cmath>
#include <exception>
#include <iomanip>
#include <iostream>
#include <limits>
#include <stdexcept>
#include <string>
#include <tuple>
#include <vector>

#include "Eigen/src/Core/Matrix.h"

int main(int /*argc*/, char ** /*argv*/) {
  std::cout << "NumCSE code: leapfrog timetepping" << std::endl;

  std::cout << "Running nunExpCvgLeapfrog()" << std::endl;

  numExpCvgLeapfrog();

  return 0;
}
