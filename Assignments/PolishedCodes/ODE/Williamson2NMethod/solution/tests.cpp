#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN

#include "doctest.h"
#include "williamson2nmethod.hpp"

// Williamson (2N) coefficients according to Carpenter
static const std::vector<double> alpha_c({0.0, -756391.0 / 934407.0,
                                          -36441873.0 / 15625000.0,
                                          -1953125.0 / 1085297.0});
static const std::vector<double> beta_c({8.0 / 141.0, 6627.0 / 2000.0,
                                         609375.0 / 1085297.0,
                                         198961.0 / 526383.0});

// A function to generate sparse matrix for testing
Eigen::SparseMatrix<double> generateTestMatrix(Eigen::Index N) {
  Eigen::SparseMatrix<double> A(N, N);
  A.reserve(Eigen::RowVectorXi::Constant(N, 3));
  A.insert(0, 0) = 2.0;
  A.insert(0, 1) = -1.0;
  for (Eigen::Index j = 1; j < N - 1; ++j) {
    A.insert(j, j - 1) = 1.0;
    A.insert(j, j) = 2.0;
    A.insert(j, j + 1) = -1.0;
  }
  A.insert(N - 1, N - 1) = 2.0;
  A.insert(N - 1, N - 2) = 1.0;
  return A;
}

TEST_SUITE("Williamson2NMethod") {
  
  TEST_CASE("Eigen::VectorXd integrateAPsi_lowmem" 
    * doctest::description("compare with straightforward implementation")) {
    Eigen::Index N = 100;
    const Eigen::SparseMatrix<double> A{generateTestMatrix(N)}; 
    std::function<double(double)> psi = [](double xi) -> double {
      return xi * (2.0 - xi);
    };
    const Eigen::VectorXd y0 = Eigen::VectorXd::Constant(N, 1.0);
    Eigen::VectorXd yT    = integrateAPsi(alpha_c, beta_c, 
                                              A, psi, 1.0, y0, 20);
    Eigen::VectorXd yT_lm = integrateAPsi_lowmem(alpha_c, beta_c, 
                                              A, psi, 1.0, y0, 20);
    CHECK((yT - yT_lm).norm() == doctest::Approx(0.).epsilon(1e-4));
  }
  
  TEST_CASE("std::complex<double> stabfnWilliamson"
    * doctest::description("compare with true solution")) {
    const std::complex<double> sol{1.507071e+00,2.333333e+00}, z{1.,1.};
    CHECK(abs(stabfnWilliamson(alpha_c, beta_c, z) - sol)
      == doctest::Approx(0.).epsilon(1e-4));  
  }
}