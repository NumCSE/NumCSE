#pragma once
/* **********************************************************************
 * Course "Numerical Methods for CSE", R. Hiptmair, SAM, ETH Zurich
 * Author: R. Hiptmair
 * Date: December 2024
 */

#define EIGEN_RUNTIME_NO_MALLOC
#include <Eigen/Dense>
#include <Eigen/Sparse>
#include <cassert>
#include <cmath>
#include <cstddef>
#include <iomanip>
#include <ios>
#include <iostream>
#include <vector>


/**
 * Implementation of Williamson (2N) scheme for scalar IVP
 *
 * @param alpha Alpha coefficients of the method, alpha[0] not used
 * @param beta Beta coefficients of the method
 * @param f functor providing the vector field of the ODE
 * @param T final time
 * @param y0 initial value
 * @param M number of equidistant time steps
 */
/* SAM_LISTING_BEGIN_1 */
template <typename STATE, typename FUNCTION_F>
std::vector<std::pair<double, STATE>> solveWilliamsonIVP(
    std::vector<double> alpha, std::vector<double> beta, FUNCTION_F &&f,
    double T, STATE y0, unsigned int M) {

  const size_t s = alpha.size(); // Number of stages
  assert((s == beta.size()) && ("size mismath of coefficient vectors"));
  const double h = T / M;  // timestep size
  STATE g{y0};             // Internal state variable
  // Sequence of approximate (extended) states $(t_k,\Vy_k)$
  std::vector<std::pair<double, STATE>> y_seq{{0.0, y0}};
  // Main timestepping loop: M timesteps
  for (unsigned int k = 0; k < M; ++k) {
    // Single step of Williamson (2N) method
    STATE p{h * f(g)};
    g += beta[0] * p;
    for (unsigned int i = 1; i < s; ++i) {
      p = alpha[i] * p + h * f(g);
      g += beta[i] * p;
    }
    // Store $t_k$ and approximate state $\Vy_k$.
    y_seq.emplace_back((k + 1) * h, g);
  }
  return y_seq;
}
/* SAM_LISTING_END_1 */

/**
 * Tabulate timestepping error of Williamson (2N) scheme for ODE y' = cos^2(y)
 * over [0,2]
 *
 * @param M_vec A sequence of numbers of timesteps to be used
 * @param alpha Alpha coefficients of the method
 * @param beta Beta coefficients of the method
 */
/* SAM_LISTING_BEGIN_2 */
void tabulateErrW2N(std::vector<unsigned int> &&M_vec,
                    std::vector<double> alpha, std::vector<double> beta) {
  // Right-hand side function of ODE
  std::function<double(double)> rhs_f = [](double y) -> double {
    const double tmp = std::cos(y);
    return tmp * tmp;
  };
  // Exact solution
  std::function<double(double)> sol_y = [](double t) -> double {
    return std::atan(t);
  };
  // Array for recording error norms
  std::vector<std::pair<unsigned int, double>> errtab;

  // TODO: (11-13.c) Tabulate error versus the number of timestep
  //
  // START
  for (unsigned int M : M_vec) {
    double err = 0.0;
    std::vector<std::pair<double, double>> yh{
        solveWilliamsonIVP(alpha, beta, rhs_f, 2.0, 0.0, M)};
    for (auto &ty : yh) {
      err = std::max(err, std::abs(sol_y(ty.first) - ty.second));
    }
    errtab.emplace_back(M, err);
  }
  // END

  std::cout << std::setw(5) << "M" << " | " << std::setw(12) << "error\n";
  std::cout << "------------------------------\n";
  for (unsigned int j = 0; j < errtab.size(); ++j) {
    std::cout << std::setw(5) << errtab[j].first << " | " << std::setw(20)
              << std::scientific << errtab[j].second << std::endl;
  }
}
/* SAM_LISTING_END_2 */


/**
 * Solve the specific vector ODE with Williamson (2N) scheme
 *
 * @param alpha Alpha coefficients of the method, alpha[0] not used
 * @param beta Beta coefficients of the method
 * @param A sparse matrix A in the rhs
 * @param psi scalar function psi in the rhs
 * @param T final time
 * @param y0 initial value
 * @param M number of equidistant time steps
 *
 */
/* SAM_LISTING_BEGIN_3 */
Eigen::VectorXd integrateAPsi(std::vector<double> alpha,
                              std::vector<double> beta,
                              const Eigen::SparseMatrix<double> &A,
                              std::function<double(double)> &psi, double T,
                              const Eigen::VectorXd &y0, unsigned int M) {
  const Eigen::Index N = A.cols();
  assert((N == A.rows()) && ("A must be square"));
  assert((N == y0.size()) && ("Wrong size of y0"));

  const size_t s = alpha.size(); // Number of stages
  assert((s == beta.size()) && ("size mismath of coefficient vectors"));
  const double h = T / M;  // timestep size
  Eigen::VectorXd g{y0};   // Internal state variable
  Eigen::VectorXd p(N);    // Auxiliary vector

  // Right-hand side function for the ODE
  auto rhs_f = [&A, &psi, N](const Eigen::VectorXd &y) -> Eigen::VectorXd {
    Eigen::VectorXd tmp{A * y};
    for (Eigen::Index i = 0; i < N; ++i) {
      tmp[i] += psi(y[i]);
    }
    return tmp;
  };
  // Main timestepping loop: M timesteps
  for (unsigned int k = 0; k < M; ++k) {
    // Single step of Williamson (2N) method
    p = h * rhs_f(g);
    g += beta[0] * p;
    for (unsigned int i = 1; i < s; ++i) {
      p = alpha[i] * p + h * rhs_f(g);
      g += beta[i] * p;
    }
  }
  return g;
}
/* SAM_LISTING_END_3 */

/**
 * A low-memory version of integrateAPsi()
 */
/* SAM_LISTING_BEGIN_5 */
Eigen::VectorXd integrateAPsi_lowmem(std::vector<double> alpha,
                                     std::vector<double> beta,
                                     const Eigen::SparseMatrix<double> &A,
                                     std::function<double(double)> &psi,
                                     double T, const Eigen::VectorXd &y0,
                                     unsigned int M) {
  const Eigen::Index N = A.cols();
  assert((N == A.rows()) && ("A must be square"));
  assert((N == y0.size()) && ("Wrong size of y0"));

  const size_t s = alpha.size(); // Number of stages
  assert((s == beta.size()) && ("size mismath of coefficient vectors"));
  const double h = T / M;  // timestep size
  Eigen::VectorXd g{y0};   // Internal state variable
  Eigen::VectorXd p(N);    // Auxiliary vector

  Eigen::internal::set_is_malloc_allowed(false);
  // From now on, memory allocation will abort execution.

  // Right-hand side function for the ODE
  auto rhs_f = [&A, &psi, N, h](const Eigen::VectorXd &y,
                                Eigen::VectorXd &res) -> void {
    // TODO: (11-13.e) Avoid temporary memory allocation.
    // Please notice that the signature of rhs\_f has changed
    // from the one in integrateAPsi().
    // The following timestepping has also changed accordingly.
    //
    // START
    for (Eigen::Index k = 0; k < A.rows(); k++) {
      for (typename Eigen::SparseMatrix<double>::InnerIterator it(A, k); it;
           ++it) {
        res[it.row()] += h * it.value() * y[it.col()];
      }
    }
    for (Eigen::Index i = 0; i < N; ++i) {
      res[i] += h * psi(y[i]);
    }
    // END
  };
  // Main timestepping loop: M timesteps
  for (unsigned int k = 0; k < M; ++k) {
    // Single step of Williamson (2N) method
    p.setZero();
    rhs_f(g, p);
    g += beta[0] * p;
    for (unsigned int i = 1; i < s; ++i) {
      p = alpha[i] * p;
      rhs_f(g, p);
      g += beta[i] * p;
    }
  }
  Eigen::internal::set_is_malloc_allowed(true);
  return g;
}
/* SAM_LISTING_END_5 */

/**
 * A low-memory version of integrateAPsi that tries to leverage Eigen's
 * expression template mechanism --> Seems not working
 */
/* SAM_LISTING_BEGIN_4 */
Eigen::VectorXd integrateAPsi_lowmem3(std::vector<double> alpha,
                                      std::vector<double> beta,
                                      const Eigen::SparseMatrix<double> &A,
                                      std::function<double(double)> &psi,
                                      double T, const Eigen::VectorXd &y0,
                                      unsigned int M) {
  const Eigen::Index N = A.cols();
  assert((N == A.rows()) && ("A must be square"));
  assert((N == y0.size()) && ("Wrong size of y0"));

  const size_t s = alpha.size(); // Number of stages
  assert((s == beta.size()) && ("size mismath of coefficient vectors"));
  const double h = T / M;  // timestep size
  Eigen::VectorXd g{y0};   // Internal state variable
  Eigen::VectorXd p(N);    // Auxiliary vector
                           // Right-hand side function for the ODE
  Eigen::internal::set_is_malloc_allowed(false);
  // From now on, memory allocation will abort execution.

  // Right-hand side function for the ODE
  auto rhs_f = [&A, &psi, N, h](const Eigen::VectorXd &y,
                                Eigen::VectorXd &res) -> void {
    res += h * A * y;  // Expression template mechanism avoids allocation?
    for (Eigen::Index i = 0; i < N; ++i) {
      res[i] += h * psi(y[i]);
    }
  };
  // Main timestepping loop: M timesteps
  for (unsigned int k = 0; k < M; ++k) {
    // Single step of Williamson (2N) method
    p.setZero();
    rhs_f(g, p);
    g += beta[0] * p;
    for (unsigned int i = 1; i < s; ++i) {
      p = alpha[i] * p;
      rhs_f(g, p);
      g += beta[i] * p;
    }
  }
  Eigen::internal::set_is_malloc_allowed(true);
  return g;
}
/* SAM_LISTING_END_4 */

/**
 * Another low-memory version of integrateAPsi()
 */
/* SAM_LISTING_BEGIN_7 */
Eigen::VectorXd integrateAPsi_lowmem2(std::vector<double> alpha,
                                     std::vector<double> beta,
                                     const Eigen::SparseMatrix<double> &A,
                                     std::function<double(double)> &psi,
                                     double T, const Eigen::VectorXd &y0,
                                     unsigned int M) {
  const Eigen::Index N = A.cols();
  assert((N == A.rows()) && ("A must be square"));
  assert((N == y0.size()) && ("Wrong size of y0"));

  const size_t s = alpha.size(); // Number of stages
  assert((s == beta.size()) && ("size mismath of coefficient vectors"));
  const double h = T / M;  // timestep size
  Eigen::VectorXd g{y0};   // Internal state variable
  Eigen::VectorXd p(N);    // Auxiliary vector

  Eigen::internal::set_is_malloc_allowed(false);
  // From now on, memory allocation will abort execution.

  // Right-hand side function for the ODE
  auto rhs_f = [&A, &psi, N, h](const Eigen::VectorXd &y,
                                Eigen::VectorXd &res) -> void {
    for (Eigen::Index i = 0; i < N; ++i) {
      res[i] += h * (A.row(i).dot(y) + psi(y[i]));
    }
  };
  // Main timestepping loop: M timesteps
  for (unsigned int k = 0; k < M; ++k) {
    // Single step of Williamson (2N) method
    p.setZero();
    rhs_f(g, p);
    g += beta[0] * p;
    for (unsigned int i = 1; i < s; ++i) {
      p = alpha[i] * p;
      rhs_f(g, p);
      g += beta[i] * p;
    }
  }
  Eigen::internal::set_is_malloc_allowed(true);
  return g;
}
/* SAM_LISTING_END_7 */

/**
 * Stability function for Williamson (2N) single-step method
 *
 * @param alpha Alpha coefficients of the method, alpha[0] not used
 * @param beta Beta coefficients of the method
 * @param z the point where the stability function is evaluated
 *
 */
/* SAM_LISTING_BEGIN_6 */
std::complex<double> stabfnWilliamson(std::vector<double> alpha,
                                      std::vector<double> beta,
                                      std::complex<double> z) {
  const size_t s = alpha.size(); // Number of stages
  std::complex<double> Sz;
  // TODO: (11-13.f) Evaluate the stability function of given Williamson (2N) method
  //
  // START
  Sz = 1.0;
  std::complex<double> p = 0.0;
  for (unsigned int i = 0; i < s; ++i) {
    p = alpha[i] * p + z * Sz;
    Sz += beta[i] * p;
  }
  // END
  return Sz;
}
/* SAM_LISTING_END_6 */
