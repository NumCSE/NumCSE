#include "williamson2nmethod.hpp"

// Williamson (2N) coefficients according to Carpenter
static const std::vector<double> alpha_c({0.0, -756391.0 / 934407.0,
                                          -36441873.0 / 15625000.0,
                                          -1953125.0 / 1085297.0});
static const std::vector<double> beta_c({8.0 / 141.0, 6627.0 / 2000.0,
                                         609375.0 / 1085297.0,
                                         198961.0 / 526383.0});
                                         
// A function to generate sparse matrix for testing
Eigen::SparseMatrix<double> generateTestMatrix(Eigen::Index N) {
  Eigen::SparseMatrix<double> A(N, N);
  A.reserve(Eigen::RowVectorXi::Constant(N, 3));
  A.insert(0, 0) = 2.0;
  A.insert(0, 1) = -1.0;
  for (Eigen::Index j = 1; j < N - 1; ++j) {
    A.insert(j, j - 1) = 1.0;
    A.insert(j, j) = 2.0;
    A.insert(j, j + 1) = -1.0;
  }
  A.insert(N - 1, N - 1) = 2.0;
  A.insert(N - 1, N - 2) = 1.0;
  return A;
}

int main(int /*argc*/, char ** /*argv*/) {
  std::cout << "Williamson 2N low-storage RK-SSM" << std::endl;
  
  // Generate error table
  tabulateErrW2N({10, 20, 40, 80, 160, 320, 640}, alpha_c, beta_c);
  std::cout << std::endl;
  // Test different implementations of integrateAPsi
  Eigen::Index N = 5;
  const Eigen::SparseMatrix<double> A{generateTestMatrix(N)}; 
  std::function<double(double)> psi = [](double xi) -> double {
    return xi * (1.0 - xi);
  };
  const Eigen::VectorXd y0 = Eigen::VectorXd::Constant(N, 1.0);
  Eigen::VectorXd yT    = integrateAPsi(alpha_c, beta_c, 
                                            A, psi, 1.0, y0, 10);
  Eigen::VectorXd yT_lm = integrateAPsi_lowmem(alpha_c, beta_c, 
                                            A, psi, 1.0, y0, 10);
  std::cout << std::fixed << std::setprecision(3);
  std::cout << "Result of straightforward implementation: \n" 
            << yT.transpose()    << std::endl;
  std::cout << "Result of low-memory implementation: \n" 
            << yT_lm.transpose() << std::endl;
  std::cout << std::endl;
  // Stability function
  std::cout << "S(3 + 2i) = " 
            << stabfnWilliamson(alpha_c, beta_c, std::complex<double>(3.,2.)) 
            << std::endl;
  return 0;
}
