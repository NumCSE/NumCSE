/* **********************************************************************
 * Course "Numerical Methods for CSE", R. Hiptmair, SAM, ETH Zurich
 * Author: R. Hiptmair
 * Date: October 2024
 */

#include <Eigen/Dense>
#include <cassert>
#include <cmath>
#include <iostream>
#include <limits>

namespace testLU {

// clang-format off
/* SAM_LISTING_BEGIN_1 */
bool testLU(const Eigen::MatrixXd &A) {
  Eigen::Index n = A.cols();
  assert(("Matrix must be square", n == A.rows()));
  auto ludec = A.lu(); // \label[line]{lut:1}
  Eigen::MatrixXd L{ludec.matrixLU().triangularView<Eigen::UnitLower>()}; // \label[line]{lut:2}
  Eigen::MatrixXd U{ludec.matrixLU().triangularView<Eigen::Upper>()};
  L.applyOnTheLeft(ludec.permutationP().inverse());
  return ((L * U - A).norm() == 0.0); // \label[line]{lut:3}
}
/* SAM_LISTING_END_1 */
// clang-format on 
  
/* SAM_LISTING_BEGIN_2 */
  bool testLU_valid(const Eigen::MatrixXd &A) {
    Eigen::Index n = A.cols(); 
    assert(("Matrix must be square", n == A.rows())); 
    auto ludec = A.lu(); 
    Eigen::MatrixXd L{ludec.matrixLU().triangularView<Eigen::UnitLower>()}; 
    Eigen::MatrixXd U{ludec.matrixLU().triangularView<Eigen::Upper>()};
    L.applyOnTheLeft(ludec.permutationP().inverse());
    const double slack_factor = 10;
    return ((L * U - A).norm() <=
	    slack_factor * std::numeric_limits<double>::epsilon() * n * n * A.norm());
  }
/* SAM_LISTING_END_2 */


}  // namespace testLU

int main(int /*argc*/, char ** /*argv*/) {
  std::cout << "Demo: Testing Eigen's LU decomposition" << std::endl;
  const int n = 10;
  // Hilbert matrix: a notoriously ill-conditioned s.p.d. matrix
  Eigen::MatrixXd A(n, n);
  for (int i = 0; i < n; ++i) {
    for (int j = 0; j < n; ++j) {
      A(i, j) = 1.0 / (1.0 + i + j);
    }
  }
  std::cout << ((testLU::testLU(A)) ? "testLU passed\n" : "testLU failed\n");
  std::cout << ((testLU::testLU_valid(A)) ? "testLU_valid passed\n"
                                          : "testLU_valid failed\n");
  return 0;
}
