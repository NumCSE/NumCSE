/* **********************************************************************
 * Course "Numerical Methods for CSE", R. Hiptmair, SAM, ETH Zurich
 * Author: R. Hiptmair
 * Date: October 2024
 */

#include <Eigen/Dense>
#include <cassert>
#include <cmath>
#include <iostream>
#include <limits>

namespace DiBandSys {
/* SAM_LISTING_BEGIN_1 */
Eigen::VectorXd bandedLSESolve(double alpha, const Eigen::VectorXd& b) {
  const int n = b.size();
  Eigen::VectorXd x(n);
  // Forward substitution: Solve $\VL\Vz = \Vb$
  int sgn = 1;
  x[0] = b[0];
  for (int i = 1; i < n; ++i, sgn *= -1) {
    x[i] = b[i] - x[i - 1];
  }
  // Backward substitution: Solve $\VU\Vx = \Vz$
  // Same vector used for ($\Vx$ and $\Vz$)
  x[n - 1] /= (1.0 + alpha * sgn);
  for (int j = n - 2; j >= 0; --j, sgn *= -1) {
    x[j] = x[j] + sgn * alpha * x[n - 1];
  }
  return x;
}
/* SAM_LISTING_END_1 */
}  // namespace DiBandSys

int main(int /*argc*/, char** /*argv*/) {
  double alpha = 0.5;
  const int n = 7;
  const Eigen::VectorXd b = Eigen::VectorXd::LinSpaced(n, 1.0, n);
  Eigen::VectorXd x = DiBandSys::bandedLSESolve(alpha, b);
  std::cout << "x = " << x.transpose() << std::endl;
  Eigen::VectorXd res(x.size());
  res[0] = b[0] - x[0] - alpha * x[n - 1];
  for (int i = 1; i < n; ++i) {
    res[i] = b[i] - x[i - 1] - x[i];
  }
  std::cout << "||res|| = " << res.norm() << std::endl;
  return 0;
}
