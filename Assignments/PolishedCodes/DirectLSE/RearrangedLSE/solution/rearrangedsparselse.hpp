/* **********************************************************************
 * Course "Numerical Methods for CSE", R. Hiptmair, SAM, ETH Zurich
 * Author: R. Hiptmair
 * Date: December 2024
 */
#include <Eigen/Dense>
#include <Eigen/Sparse>
#include <cassert>
#include <cmath>
#include <cstddef>
#include <iomanip>
#include <ios>
#include <iostream>
#include <stdexcept>
#include <vector>

/* SAM_LISTING_BEGIN_1 */
bool solveRearrangedLSE(const std::vector<Eigen::Triplet<double>> &A_COO,
                        Eigen::VectorXd &x, Eigen::VectorXd &b,
                        Eigen::Index m) {
  const Eigen::Index n = x.size();  // Size of LSE $\cob{\VA\Vx=\Vb}$
  assert((m < n) && ("m not smaller than n!"));
  assert((b.size() == n) && ("Wrong size of b"));
  Eigen::VectorXd rearr_rhs(n);  // Temporary right-hand side Vector $\cob{\Vr}$
  // TODO: (2-24.c) Implement the function that solves the rearranged problem of Ax = b
  //
  // START
  // Step I: Carry out computation \prbeqref{eq:rhsrearr}
  rearr_rhs.head(m).setZero();
  rearr_rhs.tail(n - m) = b.tail(n - m);
  // Multiply the given top part of x with the matrix:
  // $\cob{\left(\VA\right)_{:,1:m}\left(\Vx\right)_{1:m}}$
  for (const Eigen::Triplet<double> &trp : A_COO) {
    assert(((trp.col() < n) and (trp.row() < n)) &&
           ("A triplet out of bounds"));
    if (trp.col() < m) {
      rearr_rhs[trp.row()] -= trp.value() * x[trp.col()];
    }
  }

  // Step II.1: First step of block back-substitution
  // Extract bottom right block of A and store it in CCS format
  std::vector<Eigen::Triplet<double>> A_bot_COO;
  for (const Eigen::Triplet<double> &trp : A_COO) {
    if ((trp.row() >= m) and (trp.col() >= m)) {
      A_bot_COO.emplace_back(trp.row() - m, trp.col() - m, trp.value());
    }
  }
  Eigen::SparseMatrix<double> A_bot(n - m, n - m);
  A_bot.setFromTriplets(A_bot_COO.begin(), A_bot_COO.end());
  // Solve $\cob{\left( \VA \right)_{m+1:n,m+1:n} \cor{\left( \Vx\right)_{m+1:n}} = \left(\Vr \right)_{m+1:n}}$.
  const Eigen::SparseLU<Eigen::SparseMatrix<double>> solver(A_bot);
  if (solver.info() != Eigen::Success) {
    return false;
  }
  x.tail(n - m) = solver.solve(rearr_rhs.tail(n - m));

  // Step II.2: Last step of block back-substitution
  b.head(m) = -rearr_rhs.head(m);
  for (const Eigen::Triplet<double> &trp : A_COO) {
    if ((trp.col() >= m) and (trp.row() < m)) {
      b[trp.row()] += trp.value() * x[trp.col()];
    }
  }
  // END
  return true;
}
/* SAM_LISTING_END_1 */
