#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN

#include "doctest.h"
#include "rearrangedsparselse.hpp"

// Function to generate a test matrix of size N
std::vector<Eigen::Triplet<double>> generateTestMatrix(Eigen::Index N) {
  std::vector<Eigen::Triplet<double>> A_COO;
  Eigen::Index offs1 = N / 2;
  Eigen::Index offs2 = N / 3;
  for (Eigen::Index j = 0; j < N; ++j) {
    A_COO.emplace_back(j, j, 2.0);
    A_COO.emplace_back(j, (j + offs1) % N, -1.0);
    A_COO.emplace_back(j, (j + offs2) % N, 1.0);
  }
  return A_COO;
}

TEST_SUITE("RearrangedSparseLSE") {
  
  TEST_CASE("bool solveRearrangedLSE" * doctest::description("check residual of Ax = b of size 5")) {
    const Eigen::Index n = 5, m = 3;
    std::vector<Eigen::Triplet<double>> A_COO{generateTestMatrix(n)};
    Eigen::VectorXd x = Eigen::VectorXd::LinSpaced(n, 0.0, 1.0);
    Eigen::SparseMatrix<double> A(n, n);
    A.setFromTriplets(A_COO.begin(), A_COO.end());
    Eigen::VectorXd b = A * x;
    b.head(m).setZero();
    x.tail(n - m).setZero();
    
    bool flag;
    double resnorm = 1e10;
    // Call function
    if (!solveRearrangedLSE(A_COO, x, b, m)) {
      flag = false;
      std::cout << "Gaussian elimination failed!\n";
    }
    else {
      flag = true;
      // Calculate residual 
      Eigen::VectorXd res = A * x - b;
      resnorm = res.norm();
    }
    CHECK(flag);
    CHECK(resnorm == doctest::Approx(0.).epsilon(1e-4));
  }
  
  TEST_CASE("bool solveRearrangedLSE" * doctest::description("check residual of Ax = b of size 100")) {
    const Eigen::Index n = 100, m = 37;
    std::vector<Eigen::Triplet<double>> A_COO{generateTestMatrix(n)};
    Eigen::VectorXd x = Eigen::VectorXd::Random(n);
    Eigen::SparseMatrix<double> A(n, n);
    A.setFromTriplets(A_COO.begin(), A_COO.end());
    Eigen::VectorXd b = A * x;
    b.head(m).setZero();
    x.tail(n - m).setZero();
    
    bool flag;
    double resnorm = 1e10;
    // Call function
    if (!solveRearrangedLSE(A_COO, x, b, m)) {
      flag = false;
      std::cout << "Gaussian elimination failed!\n";
    }
    else {
      flag = true;
      // Calculate residual 
      Eigen::VectorXd res = A * x - b;
      resnorm = res.norm();
    }
    CHECK(flag);
    CHECK(resnorm == doctest::Approx(0.).epsilon(1e-4));
  }

}