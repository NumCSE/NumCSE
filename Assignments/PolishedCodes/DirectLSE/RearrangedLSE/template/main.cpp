#include "rearrangedsparselse.hpp"

int main(int /*argc*/, char ** /*argv*/) {
  std::cout << "Solving a rearranged LSE with m = 2, n = 3\n";
  
  std::vector<Eigen::Triplet<double>> A_COO;
  A_COO.emplace_back(0,0,2);
  A_COO.emplace_back(0,1,3);
  A_COO.emplace_back(1,1,4);
  A_COO.emplace_back(2,2,5);

  Eigen::VectorXd x(3); x << 1, 2, 3;
  Eigen::SparseMatrix<double> A(3, 3);
  A.setFromTriplets(A_COO.begin(), A_COO.end());
  Eigen::VectorXd b = A * x;
  b.head(2).setZero();
  x.tail(1).setZero();
  
  // Calculate residual 
  solveRearrangedLSE(A_COO, x, b, 2);
  Eigen::VectorXd res = A * x - b;
  std::cout << "Residual = " << res.norm() << std::endl;
  return 0;
}
