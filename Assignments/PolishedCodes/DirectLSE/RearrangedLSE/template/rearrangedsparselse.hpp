#ifndef REARRLSE
#define REARRLSE
/* **********************************************************************
 * Course "Numerical Methods for CSE", R. Hiptmair, SAM, ETH Zurich
 * Author: R. Hiptmair
 * Date: December 2024
 */
#include <Eigen/Dense>
#include <Eigen/Sparse>
#include <cassert>
#include <cmath>
#include <cstddef>
#include <iomanip>
#include <ios>
#include <iostream>
#include <stdexcept>
#include <vector>

bool solveRearrangedLSE(const std::vector<Eigen::Triplet<double>> &A_COO,
                        Eigen::VectorXd &x, Eigen::VectorXd &b,
                        Eigen::Index m) {
  const Eigen::Index n = x.size();
  assert((m < n) && ("m not smaller than n!"));
  assert((b.size() == n) && ("Wrong size of b"));
  Eigen::VectorXd rearr_rhs(n);

  // TODO: (2-24.c) Implement the function that solves the rearranged problem of Ax = b
  //
  // START

  // END
  return true;
}
#endif
