import autodiff as ad
from autodiff import show
import numpy as np
import random
import math

#READ: (8-X.a)
#START

"""
Autodiff turns each expression in to a computation graph and tracks the objects used in it.
When we call the differential operator, it follows back the computation graph and calculates the differential recursively using the chain rule.
For this, it can be helpful to drop the notion of a functions variable here. Any tensor may be a function of any other tensor. Albeit a constant one.
When we define a tensor F = 3 * x * x, we already know a concrete value for x. These expressions make use of operator overloading. 
The dependence on x is implicitly defined via assignment of the expression 3*x*x to F. 
Note how we can derive F w.r.t. y, even though y is not even known when defining F. 
It will simply not appear in the computation graph for F.
Here is the computation graph with all (intermediate) Tensor-type objects represented in brackets [] and operations in parantheses ().

           +-[x]-+   [y]
           |     |
    (3*_)<-+     |
      |          |
    [3*x]        |
      |          |
      +->(_*_)<--+
           |
          [F]
"""
def example_0d_simple():

    x = ad.Tensor(2)
    F = 3 * x * x
    y = ad.Tensor(5)

    show("F.diff(x)", locals())
    show("F.diff(y)", locals())

"""
Of course, these operations can be nested and any tensors may be differentiated.
"""
def example_0d_nested():

    x = ad.Tensor(2)
    y = ad.Tensor(5)
    F = 3 * x * x + 2 * y + 10
    G = F + y

    show("F.diff(y)", locals())
    show("G.diff(y)", locals())
    show("x.diff(x)", locals())
    show("x.diff(y)", locals())
    show("G.diff(F)", locals())

"""
The differential is a tensor itself and corresponds to DF(x) in definition 8.5.1.13.
Further, since it is implemented using tensor operations, the differential itself can be derived again.
Thus, we may obtain higher order derivatives.
"""
def example_0d_second_order( ):

    x = ad.Tensor(2)
    F = 3 * x * x

    show("F.diff(x).diff(x)", locals())

"""
Referring to definition 8.5.1.13, in this code example we take V=R^3 and W=R^2. Then the shape of DF(x) is (2,3) (aka the Jacobian (matrix) of F w.r.t. x).
Note that the space U that DF(x) maps into is still a vector space (with 6 degrees of freedom, one for each partial derivative).
There is just more structure to the way the vectors of Z are indexed than the common linear index.
Thus according to definition, for J:V -> U with J(x)=DF(x) we can also obtain DJ(x), the differential of the above Jacobian w.r.t x, with shape (2,3,3).
The interpretation is still the same: the partial derivative of J[i,j] w.r.t. to x[k] is DJ(x)[i,j,k]. 
"""
def example_shape( ):

   x = ad.Tensor([1,2,3])
   F = 1 / x[:2] # take the inverse of the first two elements of x
   J = F.diff(x)

   show("x.shape", locals())
   show("J", locals())
   show("J.shape", locals())
   show("J.diff(x)", locals())
   show("J.diff(x).shape", locals())

"""
Caution, there is however, a limitation of the (implicit) graph approach. 
Although you might expect the example below to give the partial derivative of F w.r.t to x[0], x[0] will actually create a new `Tensor` object which does not appear in the computation graph of F. 
Similarly, y will return a zero derivative, although the derivative would be in some change-of-variable sense computable and non-zero.

      [x]--------+
       |         |
(3*_)<-+->(2*_)  +->(_[0])
  |         |         |
 [F]       [y]      [x[0]]
"""
def example_false_friends( ):

   x = ad.Tensor([1,2,3])
   y = 2 * x
   F = 3 * x
   show("F.diff(x[0])", locals())
   show("F.diff(y)", locals())

#END

def newton_arctan_step(F: ad.Tensor, x: ad.Tensor) -> ad.Tensor:

    # TODO: 8-X.b)
    # START implement the general scalar Newton step using autodiff
    x1 = x - F / F.diff(x)
    # END

    return x1

def newton_arctan_stopping_criterion(x0: float, x1: float) -> bool:
    
    # TODO: 8-X.b)
    # START implement a stopping criterion for scalar Newton's method
    return abs((x1 - x0)/ x1) <= 1e-9
    # END

def newton_arctan():

    b = random.random()
    x0 = 1.3 - b
    stop = False

    def print_status():
    
        print(dict(x = x0, F = float(np.arctan(x0 + b))))
    
    while not stop:
        
        x = ad.Tensor(x0)
        F = ad.arctan(x + ad.Tensor(b))
        print_status( )

        x1 = newton_arctan_step(F, x).value
        stop = newton_arctan_stopping_criterion(x0, x1)
        x0 = x1

        if stop:
            print_status()

    return x0

def rosenbrock_init():

    a = 10 * random.random() - 5
    b = 50 + 20 * random.random()
    x0 = np.array([-3 * np.sign(a), -5])

    return a, b, x0

def newton_rosenbrock_optimize(a, b, x0):

    def rosenbrock(v):
        
        x = v[0]
        y = v[1]
        return (a - x)**2 + b*(y - x**2)**2

    stop = False

    while not stop:

        x = ad.Tensor(x0)
        L = rosenbrock(x)
        print(dict(x = str(x0), L = str(L.value)))

        #TODO: 8-X.e
        #START
        g = L.diff(x)
        H = g.diff(x)
        x1 = x.value - np.linalg.inv(H.value) @ g.value
        stop = np.linalg.norm((x1 - x0))/ np.linalg.norm(x1) <= 1e-9
        x0 = x1
        #END
        if stop:
            print(dict(x = str(x0), L = str(rosenbrock(x0))))

    return x0

def newton_rosenbrock():

    newton_rosenbrock_optimize(* rosenbrock_init())

def gradient_rosenbrock_optimize(a, b, x0):

    def rosenbrock(v):
        
        x = v[0]
        y = v[1]
        return (a - x)**2 + b*(y - x**2)**2

    stop = False
    step_size = 5e-4
    last_loss = np.inf
    t = 0

    while not stop:

        x = ad.Tensor(x0)
        L = rosenbrock(x)
        
        if t % 1000 == 0:
            print(dict(x = str(x0), L = str(L.value)))
        t+=1

        #TODO: 8-X.f
        #START
        g = L.diff(x)
        step_size = step_size * 1.1 if L.value < last_loss else step_size / 2
        x1 = x.value - step_size * g.value #gradient step
        stop = np.linalg.norm((x1 - x0)) / np.linalg.norm(x1) <= 1e-9
        x0 = x1
        #END
        
        last_loss = L.value
        if stop:
            print(dict(x = str(x0), L = str(rosenbrock(x0))))

    return x0

def gradient_rosenbrock():

    gradient_rosenbrock_optimize(* rosenbrock_init())

def deep_learning( ):

    #generate samples
    true_v = np.array([[1., -2], [8, 6]])
    true_w = np.array([[2., -5]])

    def true_fn(input):

        return np.tanh(true_w @ np.tanh(true_v @ input))

    n_samples = 2000
    indices = range(n_samples)
    noise_level = 5
    sample_inputs = [[-0.5 + 1*random.random(), -0.5 + 1*random.random()] for i in indices]
    clean_outputs = [true_fn(sample_inputs[i]) for i in indices]
    sample_outputs = [clean_outputs[i] + noise_level * random.random() - noise_level / 2 for i in indices] 
    sample_inputs = ad.Tensor(np.array(sample_inputs).reshape(n_samples, 2, 1))
    clean_outputs = ad.Tensor(np.array(clean_outputs).reshape(n_samples, 1, 1))
    sample_outputs = ad.Tensor(np.array(sample_outputs).reshape(n_samples, 1, 1))

    #define architecture
    v = ad.Tensor(np.random.rand(2, 2))
    w = ad.Tensor(np.random.rand(1, 2))
    
    def forward(input):

        return ad.tanh(w @ ad.tanh(v @ input))

    #optimize
    stop = False    

    # mean absolute error
    def error(batch_size = n_samples, forward = forward):

        assert 1 <= batch_size and batch_size <= n_samples

        idx = np.random.randint(0, n_samples - batch_size + 1)
        return ad.sum(ad.abs(forward(sample_inputs[idx:idx+batch_size,:,:]) - sample_outputs[idx:idx+batch_size,:,:])) / batch_size

    baseline_mean_error = float(error(forward = lambda *_: ad.sum(sample_outputs) / n_samples).value)
    optimal_error = float(error(forward = lambda *_: clean_outputs).value)
    expected_optimal_error = noise_level / 4 #using properties of the centered uniform distribution

    # relative mean absolute error loss
    def loss(batch_size = n_samples, forward = forward, err = None):

        if err is None:
            assert 1 <= batch_size and batch_size <= n_samples
            err = error(batch_size, forward)
        
        return (err - optimal_error)/(baseline_mean_error - optimal_error)

    print(f"uniform noise level = {noise_level}, expected optimal error = {expected_optimal_error}")
    print(f"optimal error: {optimal_error}, baseline/mean predictor error = {baseline_mean_error}")
    print(f"optimal loss: {loss(err = optimal_error)}, baseline/mean predictor loss = {loss(err = baseline_mean_error)}")

    try:

        while not stop:

            print(f"loss = {loss().value}")
            stepsize = 1
            L = loss(batch_size = 50)
            v = ad.Tensor((v - stepsize * L.diff(v)).value)
            w = ad.Tensor((w - stepsize * L.diff(w)).value)
            stop = loss().value <= 0.05

    except KeyboardInterrupt:

        print( "optimization stopped manually" )

    print(f"final loss = {loss().value}, final error = {error().value}")
    print(f"final v = { v }")
    print(f"final w = { w }")

    return w
