import unittest
import numpy as np
from autodiff import _chain_slicings
from autodiff import slicing

class TestChainSlicings(unittest.TestCase):
    
    def _test_chain_with_actual_slicing(self, shape, first, second):
        # Create a sample array with the given shape
        x = np.arange(np.prod(shape)).reshape(shape)
        
        # Perform double slicing on the array
        expected = x[first][second]
        
        # Get the result from _chain_slicings
        result_slice = _chain_slicings(shape, first, second)
        
        # Apply the chained slicing to the array
        result = x[result_slice]
        
        # Compare the actual double slicing result to the chained slicing result
        np.testing.assert_array_equal(result, expected)

    def test_basic_slice_chain(self):
        shape = (10, 10)
        first = slicing[1:8:2, :]
        second = slicing[0:3, 1:6:2]
        self._test_chain_with_actual_slicing(shape, first, second)
    
    def test_single_integer_index(self):
        shape = (10, 10)
        first = slicing[2, :]
        second = slicing[0:5:2]
        self._test_chain_with_actual_slicing(shape, first, second)
        
    def test_double_integer_index(self):
        shape = (10, 10, 10)
        first = slicing[2, 3, :]
        second = slicing[0:5:2]
        self._test_chain_with_actual_slicing(shape, first, second)

    def test_combined_slice_and_integer(self):
        shape = (10, 10, 10)
        first = slicing[2:8, 0:5, 3]
        second = slicing[1:3, 2]
        self._test_chain_with_actual_slicing(shape, first, second)

    def test_non_overlapping_slice(self):
        shape = (10, 10)
        first = slicing[1:8:3, :]
        second = slicing[0:1, 3:7:2]
        self._test_chain_with_actual_slicing(shape, first, second)

    def test_no_slice_change(self):
        shape = (10, 10)
        first = slicing[:, :]
        second = slicing[:, :]
        self._test_chain_with_actual_slicing(shape, first, second)

    def test_step_only_slice(self):
        shape = (10, 10)
        first = slicing[::2, :]
        second = slicing[:, 1:6:2]
        self._test_chain_with_actual_slicing(shape, first, second)

    def test_stop_only_slice(self):
        shape = (10, 10)
        first = slicing[1:9:1, 0:8]
        second = slicing[:3, 2:6]
        self._test_chain_with_actual_slicing(shape, first, second)

    def test_complex_slice_combination(self):
        shape = (20, 20)
        first = slicing[2:18:2, 3:17:3]
        second = slicing[1:5:2, 2:4]
        self._test_chain_with_actual_slicing(shape, first, second)

if __name__ == "__main__":
    unittest.main()
