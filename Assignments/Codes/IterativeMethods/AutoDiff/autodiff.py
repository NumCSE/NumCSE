from __future__ import annotations
from typing import Callable as Fn

import numpy as np

def main( ):

    x = Tensor([ 1, 2 ])
    y = Tensor([ 2, 4 ])
    C = Tensor( np.random.rand( 5, 3 ))
    D = Tensor( np.random.rand( 10, 3, 2 ))
    z = Tensor( np.array(( 5, )))
    B = Tensor( np.random.rand( 3, 2 ))
    
    show("x.diff( x, slicing[ 1 ], slicing[ 0 ])", locals())
    show("x[ 1 ].diff( x, None, slicing[ 1 ])", locals())
    show("x.diff( y )", locals())

    show("1./np.array(( 5 ))", locals())
    show("x", locals())
    show("1/y", locals())
    show("sum(x)", locals())
    show("x.diff(x)", locals())
    show("y", locals())
    show("5 * ( x - y / 2 )", locals())
    show("(5 * ( x - y / 2 )).diff(x)", locals())
    show("(5 * ( x - y / 2 )).diff(y)", locals())
    show("x[0].diff(x)", locals())
    show("x[0].diff(x).diff(x)", locals())
    show("x[1].diff(x)", locals())
    show("x[1].diff(x).diff(x)", locals())

    show("( 2*z*z + 3*z + 5 ).diff(z) - ( 4*z + 3 )", locals())
    show("1/z", locals())
    show("( 1/z ).diff( z ) + 1/(z*z)", locals())

    show("log( z ).diff( z ) - 1/z", locals())

    show("( C @ D ).value-C.value @ D.value", locals())
    show("( C @ D ).shape", locals())
    show("B-0.5", locals())
    show("relu(B-0.5)", locals())
    show( "( C @ D ).diff( C )", locals())


def broadcast_batch_matrix_multiplication( x: Tensor, y: Tensor ) -> Tensor:

    # optimization: optionally turn off all of these assertions to make it faster
    assert len( x.shape ) == 2 and len( y.shape ) == 3
    assert x.shape[ -1 ] == y.shape[ -2 ]
    z = np.ndarray(( y.shape[ 0 ], x.shape[ -2 ], y.shape[ -1 ]), dtype = _tensordtype )

    # einsum notation: ij,bjk->bik"
    for ( b, i, k ) in np.ndindex( z.shape ):
    
        z[ b, i, k ] = sum_arrangement( _n_ary_operation( lambda x, y: x * y, x[ i, : ], y[ b, :, k ]))
    
    return glue_arrangement( z )

def tensor_slicing( f: Tensor, slicing: [ int, slice, tuple ]) -> Tensor:

    if type( slicing ) != tuple:
        slicing = ( slicing, )

    return Tensor( f.value[ slicing ], lambda _, x, upper, lower: f.diff( x, _chain_slicings( f.shape, slicing, upper ), lower ))

def glue_arrangement( tensors: np.ndarray ) -> Tensor:

    tensors = _receive_arrangement( tensors )
    value = _get_value_of_tensor_arrangement( tensors )

    def diff_rule( f: Tensor, x: Tensor, upper: tuple, lower: tuple ) -> Tensor:

        any_tensor = tensors[( 0, ) * len( tensors.shape )]
        assert f.shape == ( * tensors.shape, * any_tensor.shape ), f"got shape { f.shape }, but expected {( * tensors.shape, * any_tensor.shape )}"

        n_arrangement_dims = len( tensors.shape )
        arrangement_slicing = upper[ :n_arrangement_dims ]
        tensor_slicing = upper[ n_arrangement_dims: ]
        return glue_arrangement( _get_diffed_tensors_of_tensor_arrangement( tensors, x, arrangement_slicing, tensor_slicing, lower ))

    return Tensor( value, diff_rule )

# element-wise, i.e. sums all elements of the arrangement together
def sum_arrangement( tensors: np.ndarray ) -> Tensor:

    tensors = _receive_arrangement( tensors )
    any_tensor = tensors[( 0, ) * len( tensors.shape )]
    
    value = np.sum( np.vectorize( lambda t: t.value, otypes = [ np.ndarray ])( tensors ))
    assert value.shape == any_tensor.shape

    def diff_rule( f: Tensor, x: Tensor, upper: tuple, lower: tuple ):

        return sum_arrangement( _get_diffed_tensors_of_tensor_arrangement( tensors, x, _get_full_slicing( tensors.shape ), upper, lower ))

    return Tensor( value, diff_rule )

def add_const( const: [ int, float ], f: Tensor ) -> Tensor:

    return Tensor( 
        const + f.value, 
        lambda _, x, upper, lower: f.diff( x, upper, lower ) 
    )

def add( u: Tensor, v: Tensor ):

    return Tensor( 
        u.value + v.value,
        lambda _, x, upper, lower: u.diff( x, upper, lower ) + v.diff( x, upper, lower )
    )

def multiply_const( const: [ int, float ], f: Tensor ) -> Tensor:

    return Tensor( 
        const * f.value,
        lambda _, x, upper, lower: const * f.diff( x, upper, lower )
    )

def multiply_scalar_scalar( u: Tensor, v: Tensor ) -> Tensor:

    assert u.is_scalar( ) and v.is_scalar( )
    
    return Tensor( 
        u.value * v.value,
        lambda _, x, upper, lower: u.diff( x, None, lower ) * v + u * v.diff( x, None, lower )
    )

def multiply_scalar_tensor( scalar: Tensor, f: Tensor ) -> Tensor:

    assert scalar.is_scalar( )
    array = np.ndarray( f.shape, dtype = _tensordtype )
    
    for idx in np.ndindex( array.shape ):
    
        array[ idx ] = scalar * f[ idx ]

    result = glue_arrangement( array )
    assert result.shape == f.shape
    return result

def pow_positive_int( f: Tensor, power: int ) -> Tensor:

    if power == 1:
        return f

    return pow_positive_int( f * f, power - 1 )

# function g must be defined on scalar np.ndarray and derivative dg on scalar Tensor
def lift_scalar_function( g: Fn[[np.ndarray], np.ndarray], dg: Fn[[Tensor], Tensor]):

    def scalar_operator( u: Tensor ):

        assert u.is_scalar( )
        
        return Tensor( 
            g( u.value ),
            lambda _, x, upper, lower: dg( u ) * u.diff( x, None, lower ) # chain rule
        )

    def lifted_function( f: Tensor ):

        return glue_arrangement( _n_ary_operation( scalar_operator, f ))

    return lifted_function

inv = lift_scalar_function( lambda x: 1 / x, lambda x: - 1 /( x ** 2 ))
log = lift_scalar_function( np.log, lambda x: 1 / x )
arctan = lift_scalar_function( np.arctan, lambda x: 1 /( 1 + x ** 2 ))
sqrt = lift_scalar_function( np.sqrt, lambda x: 1 /( 2 * sqrt( x )))
abs = lift_scalar_function( np.abs, lambda x: Tensor( 1 if x.value >= 0 else -1 ))
relu = lift_scalar_function( lambda x: x if x >= 0 else np.array(0.), lambda x: Tensor( 1 if x.value >= 0 else 0 ))
square = lift_scalar_function( lambda x: x ** 2, lambda x: 2 * x )
tanh = lift_scalar_function( np.tanh, lambda x: 1 - tanh( x ) ** 2 )
exp = lift_scalar_function( np.exp, lambda x: Tensor( np.exp( x.value )))

#like np.sum: reduces to 0-d tensor
def sum( f: Tensor ) -> Tensor:

    return sum_arrangement( _n_ary_operation( lambda el: el, f ))

def square_norm( f: Tensor ) -> Tensor:

    return sum( square( f ))

"""
implementation
-----------------------------------------------------------------------------
"""

def _n_ary_operation( operator, * tensor_list: list[ Tensor ]) -> np.ndarray:

    assert len( tensor_list ) > 0
    shape = tensor_list[ 0 ].shape
    dtype = tensor_list[ 0 ].dtype
    assert all([ shape == t.shape for t in tensor_list ])
    assert all([ dtype == t.dtype for t in tensor_list ]) #not actually necessary
    assert all([ type( t ) == Tensor for t in tensor_list ])
    
    array = np.ndarray( shape, dtype = _tensordtype )
    
    for idx in np.ndindex( array.shape ):
    
        array[ idx ] = operator( * [ t[ idx ] for t in tensor_list ])
    
    return array

def _get_diffed_tensors_of_tensor_arrangement( tensors: np.ndarray, x: Tensor, arrangement_slicing: tuple, upper: tuple, lower: tuple ) -> np.ndarray:

    tensors = _receive_arrangement( tensors )
    tensors_to_diff = _receive_arrangement( tensors[ arrangement_slicing ])

    diffed_tensors = np.ndarray( tensors_to_diff.shape, dtype = _tensordtype )

    for idx in np.ndindex( diffed_tensors.shape ):

        diffed_tensors[ idx ] = tensors_to_diff[ idx ].diff( x, upper, lower )

    return diffed_tensors

def _get_value_of_tensor_arrangement( tensors: np.ndarray ) -> np.ndarray:

    tensors = _receive_arrangement( tensors )
    any_tensor = tensors[( 0, ) * len( tensors.shape )]
    shape = any_tensor.shape
    dtype = any_tensor.dtype
    assert all([ t.dtype == dtype for t in tensors.flat ])
    assert all([ t.shape == shape for t in tensors.flat ])
    
    value = np.empty(( * tensors.shape, * shape ), dtype = dtype )
    
    for idx in np.ndindex( tensors.shape ):

        value[ idx ] = tensors[ idx ].value

    return value

def _receive_arrangement( tensors ) -> np.ndarray:

    if not isinstance( tensors, np.ndarray ):
            
        tensors = np.array( tensors, dtype = _tensordtype )

    assert type( tensors ) == np.ndarray
    assert tensors.dtype == _tensordtype
    return tensors

class Tensor:

    def __init__( self, value, rule: Fn[[ Tensor, Tensor, tuple, tuple ]] = None ):

        if not isinstance( value, np.ndarray ):
            
            value = np.array( value )

        self._value = value 
        self.dtype = value.dtype
        self._rule = rule or _zero_diff_rule

    @property
    def shape( self ):
        return self.value.shape

    @property
    def value( self ):
        return self._value

    def diff( self, x, upper: [ tuple, None ] = None, lower: [ tuple, None ] = None ):

        if upper is None:
            upper = _get_full_slicing( self.shape )
        if lower is None:
            lower = _get_full_slicing( x.shape )
        
        dx = _id_diff_rule( self, self, upper, lower ) if x is self else self._rule.__call__( self, x, upper, lower )
        assert dx.shape == _get_diff_shape( self.shape, x.shape, upper, lower ), f"diff has shape { dx.shape }, but expected { _get_diff_shape( self.shape, x.shape, upper, lower )} (self.shape = { self.shape }, x.shape = { x.shape }, upper = { upper }, lower = { lower })"
        return dx

    def is_scalar( self ):

        return np.prod( self.shape ) == 1

    def __repr__( self ):

        if self.shape == () or np.prod( self.shape ) == self.shape[0] and self.shape[0] <= 6:

            return f"Tensor({str( self.value )})"

        return f"Tensor\n{str( self.value )}"

    def __str__( self ):

        return self.__repr__( )

    def __getitem__( self, slicing ):

        return tensor_slicing( self, slicing )

    def __add__( self, other ):

        if isinstance( other, ( int, float )):

            return add_const( other, self )

        if isinstance( other, Tensor ):

            return add( self, other )

    def __radd__( self, other ):
        
        return self.__add__( other )

    def __mul__( self, other ):

        if isinstance( other, ( int, float )):

            return multiply_const( other, self )

        if isinstance( other, Tensor ):
            
            if self.is_scalar( ) and other.is_scalar( ):

                return multiply_scalar_scalar( self, other )

            if self.is_scalar( ):

                return multiply_scalar_tensor( self, other )

            if other.is_scalar( ):

                return multiply_scalar_tensor( other, self )

        assert False, "invalid multiplication"

    def __rmul__( self, other ):
        
        if isinstance( other, ( int, float )):

            return self.__mul__( other )

        return other.__mul__( self )

    def __truediv__( self, other ):

        if isinstance( other, ( int, float )):

            return self * ( 1 / other )

        if isinstance( other, Tensor ):

            return other.__rtruediv__( self )

    def __rtruediv__( self, other ):

        return other * inv( self )

    def __matmul__( self, other ):

        if len( self.shape ) == 2 and len( other.shape ) == 3:

            return broadcast_batch_matrix_multiplication( self, other )

        assert False, "unsupported matrix multiplication"

    def __rmul__( self, other ):

        return self.__mul__( other )

    def __sub__( self, other ):

        return self + ( - other )

    def __rsub__( self, other ):

        return other + ( - self )

    def __neg__( self ):

        return -1 * self

    def __equal__( self, other ):

        return self.value == other.value

    def __pow__( self, other ):

        if isinstance( other, int ) and other >= 1:

            return pow_positive_int( self, other )

        assert False, "unsupported power"

_tensordtype = np.dtype( Tensor, align = False, copy = False )

def _get_full_slicing( f_shape: tuple ):

    return ( slice( None ),) * len( f_shape )

def _get_shape_after_slicing( f_shape: tuple, slicing: tuple, filter_zeros = True ):

    n_dims = len( f_shape )
    shape = [ 0, ] * n_dims
    
    if len( slicing ) < n_dims:

        slicing = ( * slicing, * _get_full_slicing( f_shape[ len( slicing ): ]))

    for dim in range( n_dims ):

        slc = slicing[ dim ]
        if type( slc ) == int:

            shape[ dim ] = 0
        else:

            start, stop, step = slc.indices( f_shape[ dim ])
            shape[ dim ] = max( 0, ( stop - start + ( step - ( 1 if step > 0 else -1 ))) // step )

    if filter_zeros:

        shape = [ n for n in shape if n > 0 ]

    return tuple( shape )

def _get_diff_shape( f_shape: tuple, x_shape: tuple, upper: tuple, lower: tuple ):

    return ( * _get_shape_after_slicing( f_shape, upper ), * _get_shape_after_slicing( x_shape, lower ));

def _zero_diff_rule( f: Tensor, x: Tensor, upper: tuple, lower: tuple ):

    return Tensor( np.zeros( _get_diff_shape( f.shape, x.shape, upper, lower )))

def _id_diff_rule( f: Tensor, x: Tensor, upper: tuple, lower: tuple ):

    #todo: may be slow if upper and lower are sparse
    full_eye = np.reshape( np.eye( int( np.prod( f.shape ))), ( * f.shape, * f.shape ))
    return Tensor( full_eye[( * upper, * lower )])

def _chain_slicings( shape, first, second ):

    result = [ "invalid" ] * len( first ) 
    reduced_dims_offset = 0

    for dim in range( len( first )):

        if type( first[ dim ]) == int:

            reduced_dims_offset -= 1
            result[ dim ] = first[ dim ]
        
        else:

            dim_in_second = dim + reduced_dims_offset
            index_start, index_stop, index_step = first[ dim ].indices( shape[ dim ])
            slc = second[ dim_in_second ]

            if type( slc ) == int:

                index_single_second = slc
                #reduced_dims_offset += 1
                result[ dim ] = index_start + index_single_second * index_step

            else:

                slice_second = slc
                shape_after_first = _get_shape_after_slicing( shape, first )
                index_start_second, index_stop_second, index_step_second = slice_second.indices( shape_after_first[ dim_in_second ])
                start, stop, step = first[dim].start, first[dim].stop, first[dim].step

                if slice_second.start != None:
                    start = index_start + index_start_second * index_step

                if slice_second.stop != None:
                    stop = index_start + index_stop_second * index_step

                if slice_second.step != None:
                    step = index_step * index_step_second

                result[ dim ] = slice( start, stop, step )

    return tuple( result )

class __Slicing:

    def __getitem__( self, slicing ):

        if type( slicing ) != tuple:
            slicing = ( slicing, )

        return slicing

slicing = __Slicing( )
    
def show( expr, locals ):

    result = str( eval( expr, globals( ), locals ))
    
    if "\n" in result:
        print( f"\n{ expr } =\n { result }\n" )
    else:
        print( f"{ expr } = { result }" )

if __name__ == "__main__":

    main( )