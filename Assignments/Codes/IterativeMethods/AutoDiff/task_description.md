# Problem 8-X: Automatic Differentiation

We have seen in section 8.5 of the lecture document that it is necessary to symbolically (manually) derive the Jacobian to supply it to Newton's method. In practice however, this can be quite tedious, especially for more complicated functions. In this assignment, we will learn about auto-differentiation, a programming technique which lets us calculate the differential operator automatically. Also called AD, autodiff, autograd or automatic symbolic differentation, the concept is relevant since it lies at the core of (deep) learning frameworks like PyTorch, TensorFlow, Caffe, etc. Autodiff works by breaking down computations into elementary operations, with known derivatives applied recursively using the chain rule. Note this is not the same as numerical differentiation, which tries to approximate the derivative using the divided differences formula. 

*Remark*: This assumes familiarity with the theory in section 8.5: Newtons's Method In $\mathbb{R}^d$ all through 8.5.1.18.

**8-X.a)** More dimensions also call for a more powerful concept to arrange and index numbers along axes. Tensors are a generalization of scalars (0D), vectors (1D) and matrices (2D), that is, they can have an arbitrary number of index dimensions. To formalize indexing rules, we introduce the notion of shape. The shape of tensor $X$ is a list of length $k\leq0$, each entry denoting the length of the corresponding index dimension. For example, a scalar has shape $()$, a vector of size $n$ has shape $(n)$ and a $m$-by-$n$ matrix would have shape $(m,n)$. Now, thoroughly study `example_0d_simple` and `example_0d_nested` and `example_0d_second_order` as an introduction on how to use the existing autodiff implementation. 
Next, you should have have a look at `example_shape`: Note that in definition 8.5.1.13, the differential is a linear map from $V$ to $W$. In general, the shape of the differential is the concatenation of the shape of $F$ with the shape of $x$. This generalizes from the scalar case where $W=V=\mathbb{R}$ and both shapes are $()$, thus the shape of $\text{D}F(x)$ is $()$. An interesting edge case is that of scalar-valued function where we either have $W=\mathbb{R}$ and $V=\mathbb{R}^d$, thus $\text{D}F(x)$ is $(d)$ i.e. $\text{D}F(x)\in\mathbb{R}^d$ **or** we instantiate remark 8.5.1.12 such that $m=1$, so we have $\text{D}F(x)\in\mathbb{R}^{1\times d}$. This means there is a difference between $W=\mathbb{R}$ (shape $()$) and $W=\mathbb{R}^1$ (shape $(1)$), so we may resort to the more explicit shape notation from now on.

**8-X.b)** Implement Newton's method to find the zero of $F(x)=\text{arctan}(x+b)$ for $x,b\in\mathbb{R}$. Fill in the gaps in `newton_step_zero` and `newton_stopping_criterion`. Use a relative correction-based threshold of $\tau_{\text{rel}}=10^{-9}$. (See 8.2.3.4)

**8-X.c)** Optimization algorithms are concerned with finding the local/global minima of a (scalar-valued) function, often called the loss function. Here, we are only concerned with finding a single local minimum. For (twice) continuously differentiable functions $\mathcal{L}:\mathbb{R}^d\rightarrow\mathbb{R}$, the minimum $x^\star\in\mathbb{R}^d$ of $\mathcal{L}$ can be found at the zero of its derivative $F:x \mapsto \text{D}\mathcal{L}(x)$. In the field of optimization algorithms, Newton's method is called a second-order method, since it makes use of the second derivative. To see this, rewrite the multivariate Newton iteration given below (also see 8.5.1.6) in terms of $\mathcal{L}$ and its first ($\text{D}\mathcal{L}(x_0)$) and second differential ($\text{D}^2\mathcal{L}(x_0)$).

$$x_1 := x_0 - \text{D}F(x_0)^{-1}F(x_0)$$

Hint: Based on the example codes, what are the shapes of the first and second differential? Note that $L(x_0)$ has shape $()$. Also $\text{D}\mathcal{L}(x_0)$ and $\text{D}^2\mathcal{L}(x_0)$ would correspond to `L.diff(x0)` and `L.diff(x0).diff(x0)`, respectively.

**Solution**: We have $\text{D}\mathcal{L}(x_0)\in\mathbb{R}^{d}$ and $\text{D}^2\mathcal{L}(x_0)\in\mathbb{R}^{d \times d}$. It must follow that $F(x_0)=\text{D}\mathcal{L}(x_0)$. Note that by definition (8.5.1.18) $F(x)$ is called the gradient of $\mathcal{L}$ w.r.t. $x$. Note that since $\frac{\partial^2\mathcal{L}}{\partial x_i x_j}=\frac{\partial^2\mathcal{L}}{\partial x_j x_i}$ for all $i,j\leq k$, the second differential $\text{D}^2\mathcal{L}(x_0)\in\mathbb{R}^{d \times d}$ is symmetrical. It is also called the Hessian of $\mathcal{L}$. Thus the Newton iteration may be rewritten as: $x_1 := x_0 - \text{D}^2\mathcal{L}(x_0)^{-1}\text{D}\mathcal{L}(x_0)$.

**8-X.d)** Analytically derive the global minimum (and only critical point) of the Rosenbrock function $\mathcal{L}(x,y)=(a-x)^2+b(y-x^2)^2$ for $x,y,a,b\in\mathbb{R}$ and $b>0$. What is the function value of this minimum? This non-convex function is commonly used to test the performance of optimization algorithms.

**Solution**: To find the global minimum of the Rosenbrock function $\mathcal{L}(x,y) = (a - x)^2 + b(y - x^2)^2$, we first compute the partial derivatives of the function and set them to zero to identify the critical points.
To find the global minimum of the Rosenbrock function $\mathcal{L}(x,y) = (a - x)^2 + b(y - x^2)^2$, we first compute the partial derivatives of the function and set them to zero to identify the critical points. 

1. The partial derivative with respect to $x$ is:

$$
\frac{\partial \mathcal{L}}{\partial x} = -2(a - x) - 4b x (y - x^2)
$$

2. The partial derivative with respect to $y$ is:
$$
\frac{\partial \mathcal{L}}{\partial y} = 2b (y - x^2)
$$

Setting $\frac{\partial \mathcal{L}}{\partial x} = 0$ and $\frac{\partial \mathcal{L}}{\partial y} = 0$, we solve the system of equations:
$$
2b (y - x^2) = 0 \quad \Rightarrow \quad y = x^2
$$
$$
-2(a - x) - 4b x (y - x^2) = 0 \quad \Rightarrow \quad x = a
$$

Substituting $x = a$ into $y = x^2$, we get $y = a^2$.

Thus, the global minimum occurs at $(x, y) = (a, a^2)$.

The function value at the global minimum is:
$$
\mathcal{L}(a, a^2) = (a - a)^2 + b(a^2 - a^2)^2 = 0
$$


**8-X.e)** Use Newton's method to compute the global minimum of the Rosenbrock function with unknown $a,b$. To obtain the inverse of the Hessian, use the `Tensor.value` member to get a numpy array and `numpy.linalg.inverse`. Complete the missing part in `newton_rosenbrock_optimize`. Adapt the structure given in `newton_arctan_zero` for guidance. Use a relative correction-based threshold of $\tau_{\text{rel}}=10^{-9}$

**8-X.f)** We have already mentioned that Newton's method is a second order method, since it makes use of the second derivative. A natural question arises from this: how do first order methods work and compare to Newton's method? We are going to look at a well-known (family of) first order methods called (adaptive) gradient descent. The gradient descent step on a differentiable function $\mathcal{L}:\mathbb{R}^d\rightarrow\mathbb{R}$ is much simpler:

$$
x_{t+1} := x_{t} - \eta_t \text{D}\mathcal{L}(x_t)
$$.

Notice the step size $\eta\in\mathbb{R}_{\geq 0}$, which is often varied during the optimization. Here is what to expect in comparison to Newton's method: first order methods have less information about the so-called optimization landscape with which they can work. They linearly approximate the function locally and for a suffiently small step size $\eta$, the next iterate is smaller than the current. This lack of geometrical information must often be replaced by heuristics, which drive the step size. This is to prevent both oscillation/divergence in the case of a too large step size or too slow of a progress if the step size is too small. Generally one will run into either problem with a fixed step size. The intuitive, big advantage of first order methods is that iterations compute relatively fast and that they require a memory of $O(d)$ for the gradient instead of $O(d^2)$ for the Hessian if $d$ gets very large. Consider this simple heuristic scheme called the bold driver algorithm: *if the last iteration was an improvement, increase the step size by 10%, otherwise halve it*. 

Or formally $\eta_{t+1}:=\begin{cases}1.1 \eta_{t} & \text{if } \mathcal{L}(x_{t+1}) < \mathcal{L}(x_t)\\\frac {\eta_{t}} 2\end{cases}$

Use bold driver gradient descent to compute the global minimum of the Rosenbrock function with unknown $a,b$. Complete the missing part in `gradient_rosenbrock_optimize`. Adapt your implementation of `newton_rosenbrock_optimize`. Use a relative correction-based threshold of $\tau_{\text{rel}}=10^{-9}$. By observation, how does it compare to Newton's method in terms of overall computation time and number of iterations?

**Solution**: This very simple scheme takes thousands of steps for this particular problem, whereas Newton's method requires a few dozen at most. It also takes noticeable longer to find the global optimimum (approximately). This is because the Newton steps on a landscape like this are a lot "smarter". Here, once the landscape in proximity of the optimum is almost quadratic, we have almost instant convergence.
