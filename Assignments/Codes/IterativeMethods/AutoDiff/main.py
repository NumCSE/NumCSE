from solution import *
import random

def main( ):

    random.seed( 2049 )
    
    print("8-X.a)\n")
    print("example_0d_simple\n")
    example_0d_simple()
    print("\nexample_0d_nested\n")
    example_0d_nested()
    print("\nexample_0d_second_order\n")
    example_0d_second_order()
    print("\nexample_shape\n")
    example_shape( )
    print("\nexample_false_friends\n")
    example_false_friends()
    
    print("\n\n8-X.b)\n")
    print("\nnewton_arctan\n")
    newton_arctan()

    random.seed( 1337 )

    print("\n\n8-X.e)\n")
    print("\nnewton_rosenbrock\n")
    newton_rosenbrock()

    random.seed( 1337 )

    print("\n\n8-X.f)\n")
    print("\ngradient_rosenbrock\n")
    gradient_rosenbrock()

    print("\ndeep_learning\n")
    deep_learning()

if __name__ == "__main__":

    main( )