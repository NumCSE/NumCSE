import solution as TEST
import solution as solution
import random
import autodiff as ad
import numpy as np

def newton_arctan_step( ):

	for i in range( 10 ):

		x = ad.Tensor([ 1 + random.random( )])
		F = [ ad.log, ad.arctan ][ random.randint( 0, 1 )]( x )
		x1_sol = solution.newton_arctan_step( F, x )
		x1_stud = TEST.newton_arctan_step( F, x )
		assert np.linalg.norm( x1_stud.value - x1_sol.value ) < 1e-9

def newton_arctan_stopping_criterion( ):

	for i in range( 100 ):

		x0 = random.random( )
		x1 = random.random( )
		assert solution.newton_arctan_stopping_criterion( x0, x1 ) == TEST.newton_arctan_stopping_criterion( x0, x1 )

def newton_rosenbrock_optimize():

	for i in range( 10 ):

		a, b, x0 = solution.rosenbrock_init( )
		x = TEST.newton_rosenbrock_optimize( a, b, x0 )
		assert np.linalg.norm( x - np.array([ a, a**2 ])) < 1e-4

def gradient_rosenbrock_optimize():

	for i in range( 2 ):

		a, b, x0 = solution.rosenbrock_init( )
		x = TEST.gradient_rosenbrock_optimize( a, b, x0 )
		assert np.linalg.norm( x - np.array([ a, a**2 ])) < 1e-4

def test( fn ):

	random.seed( 1337 )
	fn( )
	print( f"{ fn.__name__ } passed" )

if __name__ == "__main__":

	test( newton_arctan_step )
	test( newton_arctan_stopping_criterion )
	test( newton_rosenbrock_optimize )
	test( gradient_rosenbrock_optimize )