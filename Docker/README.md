# The NumCSE development container

This is the designated development container for the NumCSE course and its numerous codes. It is based on Docker and coupled with a CMake build system. The advantage is that, apart from Docker itself, **you do not need to install anything** and the results are consistent with everyone else. Macs with an **Apple M1 chips are supported**. You can get Docker here: https://www.docker.com/products/docker-desktop/.

## Usage

To use (i.e. get a shell on) the container, simply run `enter.sh` (Linux and MacOS). This might take a **couple of minutes the first time**, as Docker downloads and builds the underlying image for you. You may have multiple shells attached at the same time. 

### Editing

The source code in this repository can still directly be edited using your favorite text editor (Sublime, vim, Atom, etc.) on your host OS, it is mapped into the container. If you want to use a full-powered IDE there are two options of editing with Visual Studio Code:

#### Browser connected to container Visual Studio Code server (recommended)

Launch the container with `enter.sh`. Run the shortcut `vscode`. In a browser on your host OS, open `http://localhost:8081`. You can now edit the NumCSE code in a full-feature Visual Studio Code IDE in the browser with all necessary extensions preinstalled and the correct workspace configured. You can run clang-tidy on a file manually by opening the command palette and executing `Clang-Tidy: Lint File`.

#### Host OS Visual Studio Code connected to container

Using [Visual Studio Code](https://code.visualstudio.com) locally (i.e. on your host OS), install the Dev Containers extension with (ID `ms-vscode-remote.remote-containers`). Use docker to launch/enter the NumCSE development container. Then in Visual Studio Code use the command `Dev Containers: Attach to Running Container`, select this container and make sure to open the provided [workspace](./docker.code-workspace) in the `Docker` directory. When asked to confirm installation of the recommended extensions for this workspace (in the container), confirm. The extensions are important to benefit from a full-feature IDE. You may have to repeat this process after the container is destroyed.

### Compiling

Switch to the `/build` directory (the shortcut `cdbuild` will help you with that by matching your current path in the source code), then use the `make` command to compile. You **do not need to run Cmake** as it is run upon launch of the container. You may, of course,  re-run Cmake manually.

### Executing

Switch to the `/numcse/bin` directory (the shortcut `cdbin` will help you with that) and execute the binaries built in the previous step. This will be mapped as `bin` to your host directly under the root of this repository. Use this directory to **inspect the plots** generated by the codes. Caution: you generally **cannot run the binaries on your host OS**. Always run them from inside the container. In order to get back to the corresponding build or source directory, use the shortcut `cdbuild` or `cdsrc`, respectively.

## Performance

To increase container performance, you may be able to increase the number of CPUs and the memory Docker is allowed to use for running its containers (On Docker Desktiop: **Preferences > Resources**). This may be important for multi-threaded benchmark codes or compiling the entire code base. Note that on MacOS and Windows, Docker requires an intermediate VM layer and does not run natively. True native performance can only be achieved on a Linux host OS.

## Code Expert tool

It is currently not possible to run the Docker-based Code Expert tool from inside the container. Please run it **directly from your host**.

## Issues

If you experience any issues related to this Docker setup, please contact the [author](mailto:heinrich.grattenthaler@sam.math.ethz.ch).
